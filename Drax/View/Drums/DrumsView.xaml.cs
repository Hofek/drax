﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Drax.View.Drums
{
    /// <summary>
    /// Interaction logic for DrumsView.xaml
    /// </summary>
    public partial class DrumsView : UserControl
    {
        public DrumsView()
        {
            InitializeComponent();
        }
    }
}
