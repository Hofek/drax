﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace Drax.Model
{
    public class Config
    {
        public static Config Instance { get; } = new Config();
        static Config()
        {
            try
            {
                using (StreamReader sr = new StreamReader("configuration.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Config));
                    Instance = (Config)serializer.Deserialize(sr);
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine($"Nie można odczytać pliku xml. {ex.Message}");
            }            
        }
        public string Hostname { get; set; } = "127.0.0.1";
        public int Port { get; set; } = 3000;
    }
}
