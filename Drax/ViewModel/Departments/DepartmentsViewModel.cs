﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Departments
{
    public class DepartmentsViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Department> _departments;

        public DepartmentsViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            

            _departments = _databaseService.GetDepartments();
            Departments = CollectionViewSource.GetDefaultView(_departments);

            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (!(e.Value is Department department))
                return;

            Application.Current.Dispatcher.Invoke(() => Departments.Refresh());
        }

        private void LoadCommands()
        {
            AddDepartmentCommand = new UiCommand(AddDepartment);
            EditDepartmentCommand = new UiCommand(EditDepartment);
            DeleteDepartmentCommand = new UiCommand(DeleteDepartment);
        }

        private void DeleteDepartment(object obj)
        {
            if (!(obj is Department department))
                return;

            _databaseService.RemoveDepartment(department);
        }

        private void EditDepartment(object obj)
        {
            if (!(obj is Department department))
                return;

            NavigationService.WindowManager.CreateDialog(new DepartmentWizardViewModel(_databaseService, department));
        }

        private void AddDepartment(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new DepartmentWizardViewModel(_databaseService));
        }

        public UiCommand AddDepartmentCommand { get; set; }
        public UiCommand EditDepartmentCommand { get; set; }
        public UiCommand DeleteDepartmentCommand { get; set; }

        public ICollectionView Departments { get; private set; }
    }
}
