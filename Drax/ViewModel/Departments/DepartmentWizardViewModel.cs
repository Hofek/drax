﻿using Drax.Helpers;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.Departments
{
    public class DepartmentWizardViewModel : OkCancelViewModelBase
    {
        private IDatabaseService _dbService;
                
        private Department _department;
        private List<Person> _people;

        private string _name = String.Empty;                
        private Person _selectedPerson;

        public DepartmentWizardViewModel(IDatabaseService dbService, Department department = null) : base("Zatwierdź")
        {
            _department = department;
            _dbService = dbService;
            
            
            _people = _dbService.GetPeople();
            People = CollectionViewSource.GetDefaultView(_people);
            
            if (department != null)
            {
                _department = department;                   
                _name = department.Name;
                _selectedPerson = _people.FirstOrDefault(p => p.Id == department.Supervisor.Id);
            }                        
        }                

        public override bool CanOk(object obj)
        {
            if (_name.Length > 0 && _selectedPerson != null)
                return true;
            return false;
        }

        public override bool Ok(object obj)
        {
            if (_department == null)
            {
                _department = new Department
                {
                    Name = _name,
                    Supervisor = _selectedPerson                    
                };


                if ((_department.Id = _dbService.AddDepartment(_department)) > -1)
                    return true;
                else
                {
                    _department = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _department.Name = Name;
                _department.Supervisor = _selectedPerson;

                if (_dbService.EditDepartment(_department) > -1)
                    return true;
                else
                {                    
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się zedytować wpisu!");
                    return false;
                }
                    
            }

        }        

        public string Name { get => _name; set => Set(ref _name, value); }
        public Person SelectedPerson { get => _selectedPerson; set => Set(ref _selectedPerson, value); }
        public ICollectionView People { get; }
    }
}
