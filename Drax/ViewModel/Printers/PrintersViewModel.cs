﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Printers
{
    public class PrintersViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Printer> _printers;
        private List<PrinterModel> _printerModels;

        public PrintersViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            
            _printers = _databaseService.GetPrinters();
            _printerModels = _databaseService.GetPrinterModels();

            Printers = CollectionViewSource.GetDefaultView(_printers);
            PrinterModels = CollectionViewSource.GetDefaultView(_printerModels);
            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (e.Value is Printer printer)
            {
                Application.Current.Dispatcher.Invoke(() => Printers.Refresh());
            }
            else if (e.Value is PrinterModel printerModel)
            {
                Application.Current.Dispatcher.Invoke(() => PrinterModels.Refresh());
            }
        }

        private void LoadCommands()
        {
            AddPrinterCommand = new UiCommand(AddPrinter);
            EditPrinterCommand = new UiCommand(EditPrinter);
            DeletePrinterCommand = new UiCommand(DeletePrinter);

            AddPrinterModelCommand = new UiCommand(AddPrinterModel);
            EditPrinterModelCommand = new UiCommand(EditPrinterModel);
            DeletePrinterModelCommand = new UiCommand(DeletePrinterModel);
        }

        private void DeletePrinterModel(object obj)
        {
            if (!(obj is PrinterModel printerModel))
                return;

            _databaseService.RemovePrinterModel(printerModel);
        }

        private void EditPrinterModel(object obj)
        {
            if (!(obj is PrinterModel printerModel))
                return;

            NavigationService.WindowManager.CreateDialog(new PrinterModelWizardViewModel(_databaseService, printerModel));
        }

        private void AddPrinterModel(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new PrinterModelWizardViewModel(_databaseService));
        }

        private void DeletePrinter(object obj)
        {
            if (!(obj is Printer printer))
                return;

            _databaseService.RemovePrinter(printer);
        }

        private void EditPrinter(object obj)
        {
            if (!(obj is Printer printer))
                return;

            NavigationService.WindowManager.CreateDialog(new PrinterWizardViewModel(_databaseService, printer));
        }

        private void AddPrinter(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new PrinterWizardViewModel(_databaseService));
        }

        public UiCommand AddPrinterCommand { get; set; }
        public UiCommand DeletePrinterCommand { get; set; }
        public UiCommand EditPrinterCommand { get; set; }
        public UiCommand AddPrinterModelCommand { get; set; }
        public UiCommand DeletePrinterModelCommand { get; set; }
        public UiCommand EditPrinterModelCommand { get; set; }
        public ICollectionView Printers { get; }
        public ICollectionView PrinterModels { get; }
    }
}
