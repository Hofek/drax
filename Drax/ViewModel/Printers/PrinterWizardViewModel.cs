﻿using Drax.Helpers;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows.Data;

namespace Drax.ViewModel.Printers
{
    public class PrinterWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private string _name = String.Empty;
        private string _description = String.Empty;
        private IPAddress _ip;

        private List<PrinterModel> _printerModels;
        private List<Place> _places;

        private PrinterModel _selectedPrinterModel;
        private Place _selectedPlace;

        private Printer _printer;

        public PrinterWizardViewModel(IDatabaseService databaseService, Printer printer = null) : base("Zapisz")
        {
            _databaseService = databaseService;                       

            _printerModels = _databaseService.GetPrinterModels();
            _places = _databaseService.GetPlaces();

            if (printer != null)
            {
                _printer = printer;

                Name = printer.Name;
                Description = printer.Description;
                Ip = printer.Ip;
                _selectedPrinterModel = _printerModels.FirstOrDefault(p => p.Id == printer.Model.Id);
                _selectedPlace = _places.FirstOrDefault(p => p.Id == printer.Place.Id);
            }

            PrinterModels = CollectionViewSource.GetDefaultView(_printerModels);
            Places = CollectionViewSource.GetDefaultView(_places);
        }        

        public override bool CanOk(object obj)
        {
            if (_name.Length > 0 && _selectedPrinterModel != null && _selectedPlace != null && _ip != null)
                return true;
            return false;
        }                

        public override bool Ok(object obj)
        {
            if (_printer == null)
            {
                _printer = new Printer
                {
                    Name = _name,
                    Model = _selectedPrinterModel,
                    Description = _description,
                    Place = _selectedPlace,
                    Ip = _ip                    
                };

                if ((_printer.Id = _databaseService.AddPrinter(_printer)) > -1)
                {
                    return true;
                }
                else
                {
                    _printer = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _printer.Name = _name;
                _printer.Model = _selectedPrinterModel;
                _printer.Description = _description;
                _printer.Place = _selectedPlace;
                _printer.Ip = _ip;

                if (_databaseService.EditPrinter(_printer) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }
        
        public string Name { get => _name; set => Set (ref _name, value); }
        public string Description { get => _description; set => Set(ref _description, value); }
        public ICollectionView PrinterModels { get; }
        public ICollectionView Places { get; }
        public PrinterModel SelectedPrinterModel { get => _selectedPrinterModel; set => Set(ref _selectedPrinterModel, value); }
        public Place SelectedPlace { get => _selectedPlace; set => Set(ref _selectedPlace, value); }
        public IPAddress Ip { get => _ip; set => Set(ref _ip, value); }
    }
}
