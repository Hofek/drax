﻿using Drax.Helpers;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;

namespace Drax.ViewModel.Printers
{
    public class PrinterModelWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<DrumModel> _drumModels;
        private List<TonerModel> _tonerModels;
        private List<DrumModel> _compatibleDrumModels;
        private List<TonerModel> _compatibleTonerModels;
        private PrinterModel _printerModel;

        private DrumModel _selectedDrumModel;
        private TonerModel _selectedTonerModel;

        private string _name = String.Empty;
        private bool _isColorSupported;

        public PrinterModelWizardViewModel(IDatabaseService databaseService, PrinterModel printerModel = null)
        {
            _databaseService = databaseService;
            
            LoadCommands();

            _drumModels = _databaseService.GetDrumModels();
            _tonerModels = _databaseService.GetTonerModels();
           
            if (printerModel != null)
            {
                _printerModel = printerModel;
                Name = printerModel.Name;
                IsColorSupported = printerModel.IsColorSupported;
                _compatibleDrumModels = databaseService.GetCompatibleDrums(_printerModel.Id);
                _compatibleTonerModels = databaseService.GetCompatibleToners(_printerModel.Id);
            }
            else
            {
                _compatibleDrumModels = new List<DrumModel>();
                _compatibleTonerModels = new List<TonerModel>();
            }

            DrumModels = CollectionViewSource.GetDefaultView(_drumModels);
            CompatibleDrumModels = CollectionViewSource.GetDefaultView(_compatibleDrumModels);
            TonerModels = CollectionViewSource.GetDefaultView(_tonerModels);
            CompatibleTonerModels = CollectionViewSource.GetDefaultView(_compatibleTonerModels);
        }

        private void LoadCommands()
        {
            AddCompatibleDrumModelCommand = new UiCommand(AddCompatibleDrumModel, CanAddCompatibleDrumModel);
            AddCompatibleTonerModelCommand = new UiCommand(AddCompatibleTonerModel, CanAddCompatibleTonerModel);
            DeleteCompatibleDrumModelCommand = new UiCommand(DeleteCompatibleDrumModel);
            DeleteCompatibleTonerModelCommand = new UiCommand(DeleteCompatibleTonerModel);            
        }

        private void DeleteCompatibleTonerModel(object obj)
        {
            if (!(obj is TonerModel tonerModel))
                return;

            _compatibleTonerModels.Remove(tonerModel);
            CompatibleDrumModels.Refresh();
        }

        private void DeleteCompatibleDrumModel(object obj)
        {
            if (!(obj is DrumModel drumModel))
                return;

            _compatibleDrumModels.Remove(drumModel);
            CompatibleDrumModels.Refresh();
        }       

        public override bool CanOk(object obj)
        {
            if (_name.Length > 0)
                return true;
            return false;
        }

        public override bool Ok(object obj)
        {
            if (_printerModel == null)
            {
                _printerModel = new PrinterModel
                {
                    Name = _name,
                    Drums = _compatibleDrumModels,
                    Toners = _compatibleTonerModels,
                    IsColorSupported = _isColorSupported
                };
                if ((_printerModel.Id = _databaseService.AddPrinterModel(_printerModel)) > -1)
                {
                    return true;
                }
                else
                {
                    _printerModel = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _printerModel.Name = _name;
                _printerModel.Drums = _compatibleDrumModels;
                _printerModel.Toners = _compatibleTonerModels;
                _printerModel.IsColorSupported = _isColorSupported;
                if (_databaseService.EditPrinterModel(_printerModel) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }

        private bool CanAddCompatibleTonerModel(object obj)
        {
            if (_selectedTonerModel != null && !_compatibleTonerModels.Contains(_selectedTonerModel))
                return true;
            return false;
        }

        private void AddCompatibleTonerModel(object obj)
        {
            _compatibleTonerModels.Add(_selectedTonerModel);
            CompatibleTonerModels.Refresh();
            SelectedTonerModel = null;
        }

        private void AddCompatibleDrumModel(object obj)
        {
            _compatibleDrumModels.Add(_selectedDrumModel);
            CompatibleDrumModels.Refresh();
            SelectedDrumModel = null;
        }

        private bool CanAddCompatibleDrumModel(object obj)
        {
            if (_selectedDrumModel != null && !_compatibleDrumModels.Contains(_selectedDrumModel))
                return true;
            return false;
        }

        public UiCommand AddCompatibleDrumModelCommand { get; set; }
        public UiCommand AddCompatibleTonerModelCommand { get; set; }
        public UiCommand DeleteCompatibleDrumModelCommand { get; set; }
        public UiCommand DeleteCompatibleTonerModelCommand { get; set; }        

        public string Name { get => _name; set => Set(ref _name, value); }
        public PrinterModel PrinterModel { get => _printerModel; set => Set(ref _printerModel,  value); }        
        public ICollectionView DrumModels { get; }
        public ICollectionView TonerModels { get; }
        public ICollectionView CompatibleDrumModels { get; }
        public ICollectionView CompatibleTonerModels { get; }
        public bool IsColorSupported { get => _isColorSupported; set => Set(ref _isColorSupported, value); }
        public TonerModel SelectedTonerModel { get => _selectedTonerModel; set => Set(ref _selectedTonerModel, value); }
        public DrumModel SelectedDrumModel { get => _selectedDrumModel; set => Set(ref _selectedDrumModel, value); }
    }
}
