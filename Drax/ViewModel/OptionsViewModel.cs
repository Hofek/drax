﻿using Drax.Model;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using System;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace Drax.ViewModel
{
    public class OptionsViewModel : OkCancelViewModelBase
    {        
        private string _hostname = Config.Instance.Hostname;
        private int _port = Config.Instance.Port;        

        public string Hostname { get => _hostname; set => Set(ref _hostname, value); }
        public int Port { get => _port; set => Set(ref _port, value); }

        public event EventHandler ConfigUpdated;

        public override bool CanOk(object obj)
        {
            if (_hostname.Length > 0)
                return true;
            return false;
        }
        public override bool Ok(object obj)
        {
            Config.Instance.Hostname = _hostname;
            Config.Instance.Port = _port;           

            try
            {
                using (var writer = new StreamWriter("configuration.xml"))
                {
                    var serializer = new XmlSerializer(typeof(Config));
                    serializer.Serialize(writer, Config.Instance);
                }
                NavigationService.WindowManager.CreateMessageBox("Settings saved!");
                ConfigUpdated?.Invoke(this, EventArgs.Empty);
                return true;
            }
            catch
            {
                NavigationService.WindowManager.CreateMessageBox("Failed to save settings!");
                return false;
            }            
        }
    }
}
