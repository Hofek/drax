﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.People
{
    public class PeopleViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Person> _people;

        public PeopleViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            

            _people = _databaseService.GetPeople();
            People = CollectionViewSource.GetDefaultView(_people);

            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (!(e.Value is Person person))
                return;

            Application.Current.Dispatcher.Invoke(() => People.Refresh());
        }

        private void LoadCommands()
        {
            AddPersonCommand = new UiCommand(AddPerson);
            EditPersonCommand = new UiCommand(EditPerson);
            DeletePersonCommand = new UiCommand(DeletePerson);
        }

        private void DeletePerson(object obj)
        {
            if (!(obj is Person person))
                return;

            _databaseService.RemovePerson(person);
        }

        private void EditPerson(object obj)
        {
            if (!(obj is Person person))
                return;

            NavigationService.WindowManager.CreateDialog(new PersonWizardViewModel(_databaseService, person));
        }

        private void AddPerson(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new PersonWizardViewModel(_databaseService));
        }

        public UiCommand AddPersonCommand { get; set; }
        public UiCommand EditPersonCommand { get; set; }
        public UiCommand DeletePersonCommand { get; set; }

        public ICollectionView People { get; }
    }
}
