﻿using Drax.Helpers;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.People
{
    public class PersonWizardViewModel : OkCancelViewModelBase
    {
        private IDatabaseService _dbService;
        
        private Person _person;
        private string _name = String.Empty;
        private string _surname = String.Empty;

        private List<Department> _departments;
        private Department _selectedDepartment;
        public PersonWizardViewModel(IDatabaseService dbService, Person person = null)
        {
            _dbService = dbService;            
            _departments = _dbService.GetDepartments();

            if (person != null)
            {
                _person = person;
                _name = person.Name;
                _surname = person.Surname;
                _selectedDepartment = _departments.FirstOrDefault(d => d.Id == person.Department?.Id);                
            }
            Departments = CollectionViewSource.GetDefaultView(_departments);            
        }                

        public override bool CanOk(object obj)
        {
            if (_name.Length > 0 && _surname.Length > 0)
                return true;
            return false;
        }

        public override bool Ok(object obj)
        {
            if (_person == null)
            {
                _person = new Person
                {
                    Name = _name,
                    Surname = _surname,
                    Department = _selectedDepartment
                };


                if ((_person.Id = _dbService.AddPerson(_person)) > -1)
                    return true;
                else
                {
                    _person = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _person.Name = _name;
                _person.Surname = _surname;
                _person.Department = _selectedDepartment;

                if (_dbService.EditPerson(_person) > -1)
                    return true;
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się zedytować wpisu!");
                    return false;
                }
                    
            }

        }
        
        public string Name { get => _name; set => Set(ref _name, value); }
        public string Surname { get => _surname; set => Set(ref _surname, value); }                
        public Department SelectedDepartment { get => _selectedDepartment; set => Set(ref _selectedDepartment, value); }
        public ICollectionView Departments { get; }
    }
}
