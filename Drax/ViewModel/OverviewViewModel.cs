﻿using Drax.Helpers;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel
{
    public class OverviewViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        private List<Printer> _printers;
        private List<IMaterial> _materials;
        private bool _isInWarningMode;

        public OverviewViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            
            LoadData();
            Printers = CollectionViewSource.GetDefaultView(_printers);
            Materials = CollectionViewSource.GetDefaultView(_materials);
        }      

        private void DatabaseService_DataChanged(object sender, DraxCommon.Model.Args.DataChangedEventArgs e)
        {
           if (e.Value is IMaterial || e.Value is Printer)
            {
                LoadData();
                Application.Current.Dispatcher.Invoke(() => Printers.Refresh());
                Application.Current.Dispatcher.Invoke(() => Materials.Refresh());
            }
        }

        private void LoadData()
        {
            _materials = new List<IMaterial>();
            _printers = _databaseService.GetPrinters();

            var tonerModels = _databaseService.GetTonerModels();
            var drumModels = _databaseService.GetDrumModels();
            var status = _databaseService.GetPrinterStatus();

            foreach (var printer in _printers)
                if (status.FirstOrDefault(s => s.Printer.Ip.ToString() == printer.Ip.ToString()) != null)
                    printer.Status = status.FirstOrDefault(s => s.Printer.Ip.ToString() == printer.Ip.ToString());

            IsInWarningMode = false;

            foreach (var t in tonerModels)
            {
                t.Count = _databaseService.GetToners().Where(tm => tm.Model.Id == t.Id && tm.Order == null).Count();
                if (t.Count <= t.Threshold)
                    IsInWarningMode = true;
                _materials.Add(t);
            }
                
            foreach (var d in drumModels)
            {
                d.Count = _databaseService.GetDrums().Where(dm => dm.Model.Id == d.Id && dm.Order == null).Count();
                if (d.Count <= d.Threshold)
                    IsInWarningMode = true;
                _materials.Add(d);
            }
                
        }        

        public ICollectionView Printers { get; }
        public ICollectionView Materials { get; }
        public bool IsInWarningMode { get => _isInWarningMode; set => Set(ref _isInWarningMode, value); }
    }
}
