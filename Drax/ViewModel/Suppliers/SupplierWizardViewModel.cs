﻿using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;

namespace Drax.ViewModel.Suppliers
{
    public class SupplierWizardViewModel : OkCancelViewModelBase
    {
        private IDatabaseService _dbService;
        
        private Supplier _supplier;
        private string _name = String.Empty;
        private string _email = String.Empty;
        private string _phone = String.Empty;
        private string _description;

        public SupplierWizardViewModel(IDatabaseService dbService, Supplier supplier = null)
        {
            _dbService = dbService;
            
            if (supplier != null)
            {
                _supplier = supplier;
                _name = supplier.Name;
                _email = supplier.Email;
                _phone = supplier.Phone;
                _description = supplier.Description;
            }         
        }               
        public override bool CanOk(object obj)
        {
            if (_name.Length > 0 && _email.Length > 0)
                return true;
            return false;
        }

        public override bool Ok(object obj)
        {
            if (_supplier == null)
            {
                _supplier = new Supplier
                {
                    Name = _name,
                    Email = _email,
                    Phone = _phone,
                    Description = _description
                };


                if ((_supplier.Id = _dbService.AddSupplier(_supplier)) > -1)
                    return true;
                else
                {
                    _supplier = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _supplier.Name = _name;
                _supplier.Description = _description;
                _supplier.Email = _email;
                _supplier.Phone = _phone;

                if (_dbService.EditSupplier(_supplier) > -1)
                    return true;
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się zedytować wpisu!");
                    return false;
                }                    
            }
        }       

        public string Name { get => _name; set => Set(ref _name, value); }
        public string Email { get => _email; set => Set(ref _email, value); }
        public string Phone { get => _phone; set => Set(ref _phone, value); }
        public string Description { get => _description; set => Set(ref _description, value); }
    }
}
