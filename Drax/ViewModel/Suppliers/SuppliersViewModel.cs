﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace Drax.ViewModel.Suppliers
{
    public class SuppliersViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Supplier> _suppliers;

        public SuppliersViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            

            _suppliers = _databaseService.GetSuppliers();
            Suppliers = CollectionViewSource.GetDefaultView(_suppliers);


            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (!(e.Value is Supplier))
                return;            

            Application.Current.Dispatcher.Invoke(() => Suppliers.Refresh());            
        }

        private void LoadCommands()
        {
            AddSupplierCommand = new UiCommand(AddSupplier);
            EditSupplierCommand = new UiCommand(EditSupplier);
            DeleteSupplierCommand = new UiCommand(DeleteSupplier);
        }

        private void DeleteSupplier(object obj)
        {
            if (!(obj is Supplier supplier))
                return;

            _databaseService.RemoveSupplier(supplier);
        }

        private void EditSupplier(object obj)
        {
            if (!(obj is Supplier supplier))
                return;

            NavigationService.WindowManager.CreateDialog(new SupplierWizardViewModel(_databaseService, supplier));
        }

        private void AddSupplier(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new SupplierWizardViewModel(_databaseService));
        }

        public UiCommand AddSupplierCommand { get; set; }
        public UiCommand EditSupplierCommand { get; set; }
        public UiCommand DeleteSupplierCommand { get; set; }

        public ICollectionView Suppliers { get; }
    }
}
