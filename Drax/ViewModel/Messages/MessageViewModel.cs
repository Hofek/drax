﻿using Drax.Helpers;
using Drax.ViewModel.Dialogs;

namespace Drax.ViewModel.Messages
{
    public class MessageViewModel : DialogViewModelBase
    {
        private readonly string _message;
        public MessageViewModel(string message)
        {            
            _message = message;
            LoadCommands();
        }

        private void LoadCommands()
        {
            ConfirmCommand = new UiCommand(Confirm);
        }

        private void Confirm(object obj)
        {
            DialogConfirm();
        }

        public UiCommand ConfirmCommand { get; set; }
        public string Message => _message;
    }
}
