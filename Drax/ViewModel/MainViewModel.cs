﻿using Drax.Helpers;
using Drax.Services;
using Drax.View.Departments;
using Drax.ViewModel.Departments;
using Drax.ViewModel.Dialogs;
using Drax.ViewModel.Drums;
using Drax.ViewModel.Invoices;
using Drax.ViewModel.Orders;
using Drax.ViewModel.People;
using Drax.ViewModel.Places;
using Drax.ViewModel.Printers;
using Drax.ViewModel.Suppliers;
using Drax.ViewModel.Toners;
using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;

namespace Drax.ViewModel
{
    public class MainViewModel : WindowViewModelBase
    {
        IDatabaseService _dbService;  
        
        private OverviewViewModel _overviewViewModel;        
        private ViewModelBase _currentViewModel;

        public MainViewModel()
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return;

            if (!File.Exists("configuration.xml"))
                if (NavigationService.WindowManager.CreateDialog(new OptionsViewModel()) == false)
                    Application.Current.Shutdown();

            CurrentViewModel = new ConnectViewModel();

            _dbService = new DatabaseService();
            _dbService.Connected += DbService_Connected;
            _dbService.Disconnected += DbService_Disconnected;
                                  
            LoadCommands();
        }

        private void DbService_Disconnected(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => CurrentViewModel = new ConnectViewModel());
        }

        private void DbService_Connected(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => 
            {
                _overviewViewModel = new OverviewViewModel(_dbService);
                CurrentViewModel = _overviewViewModel;
            });            
        }

        private void LoadCommands()
        {
            SwitchToOverviewCommand = new UiCommand(SwitchToOverview);
            SwitchToDrumsCommand = new UiCommand(SwitchToDrums);
            SwitchToTonersCommand = new UiCommand(SwitchToToners);
            SwitchToPrintersCommand = new UiCommand(SwitchToPrinters);
            SwitchToInvoicesCommand = new UiCommand(SwitchToInvoices);
            SwitchToPlacesCommand = new UiCommand(SwitchToPlaces);
            SwitchToDepartmentsCommand = new UiCommand(SwitchToDepartments);
            SwitchToOrdersCommand = new UiCommand(SwitchToOrders);
            SwitchToPeopleCommand = new UiCommand(SwitchToPeople);
            SwitchToSuppliersCommand = new UiCommand(SwitchToSuppliers);
            OptionsCommand = new UiCommand(OpenSettings);
        }

        private void OpenSettings(object obj)
        {
            using (var optionsVm = new OptionsViewModel())
            {
                optionsVm.ConfigUpdated += OptionsVm_ConfigUpdated;
                NavigationService.WindowManager.CreateDialog(optionsVm);
            }
        }

        private void OptionsVm_ConfigUpdated(object sender, EventArgs e)
        {
            _dbService.Connected -= DbService_Connected;
            _dbService.Disconnected -= DbService_Disconnected;
            _dbService?.Dispose();

            Application.Current.Dispatcher.Invoke(() => CurrentViewModel = new ConnectViewModel());

            _dbService = new DatabaseService();
            _dbService.Connected += DbService_Connected;
            _dbService.Disconnected += DbService_Disconnected;            
        }

        private void SwitchToSuppliers(object obj)
        {
            CurrentViewModel = new SuppliersViewModel(_dbService);
        }

        private void SwitchToPeople(object obj)
        {
            CurrentViewModel = new PeopleViewModel(_dbService);
        }

        private void SwitchToOrders(object obj)
        {
            CurrentViewModel = new OrdersViewModel(_dbService);
        }

        private void SwitchToDepartments(object obj)
        {
            CurrentViewModel = new DepartmentsViewModel(_dbService);
        }

        private void SwitchToOverview(object obj)
        {
            CurrentViewModel = new OverviewViewModel(_dbService);
        }

        private void SwitchToDrums(object obj)
        {
            CurrentViewModel = new DrumsViewModel(_dbService);
        }

        private void SwitchToToners(object obj)
        {
            CurrentViewModel = new TonersViewModel(_dbService);
        }

        private void SwitchToPrinters(object obj)
        {
            CurrentViewModel = new PrintersViewModel(_dbService);
        }

        private void SwitchToInvoices(object obj)
        {
            CurrentViewModel = new InvoicesViewModel(_dbService);
        }

        private void SwitchToPlaces(object obj)
        {
            CurrentViewModel = new PlacesViewModel(_dbService);
        }

        public UiCommand SwitchToOverviewCommand { get; set; }
        public UiCommand SwitchToTonersCommand { get; set; }
        public UiCommand SwitchToPlacesCommand { get; set; }
        public UiCommand SwitchToDrumsCommand { get; set; }
        public UiCommand SwitchToPrintersCommand { get; set; }
        public UiCommand SwitchToInvoicesCommand { get; set; }
        public UiCommand SwitchToDepartmentsCommand { get; set; }
        public UiCommand SwitchToOrdersCommand { get; set; }
        public UiCommand SwitchToPeopleCommand { get; set; }
        public UiCommand SwitchToSuppliersCommand { get; set; }
        public UiCommand OptionsCommand { get; set; }

        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                var oldVM = _currentViewModel;                

                if (!Set(ref _currentViewModel, value))
                    return;

                if (oldVM != null)
                    oldVM.Dispose();
            }
        }
    }
}
