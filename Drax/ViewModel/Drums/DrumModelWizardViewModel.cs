﻿using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;

namespace Drax.ViewModel.Drums
{
    public class DrumModelWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;        
        private DrumModel _drumModel;

        private string _name = String.Empty;
        private int _threshold;

        public DrumModelWizardViewModel(IDatabaseService databaseService, DrumModel drumModel = null)
        {
            _databaseService = databaseService;
            
            if (drumModel != null)
            {
                _drumModel = drumModel;
                Threshold = drumModel.Threshold;
                Name = drumModel.Name;                                         
            }           
        }
        
        public override bool CanOk(object obj)
        {
            if (_name.Length > 0)
                return true;

            return false;
        }

        public override bool Ok(object obj)
        {
            if (_drumModel == null)
            {
                _drumModel = new DrumModel
                {
                    Name = _name,
                    Threshold = _threshold
                };
                if ((_drumModel.Id = _databaseService.AddDrumModel(_drumModel)) > -1)
                {
                    return true;
                }
                else
                {
                    _drumModel = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _drumModel.Name = _name;
                _drumModel.Threshold = _threshold;
                if ((_drumModel.Id = _databaseService.EditDrumModel(_drumModel)) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }                          

        public string Name { get => _name; set => Set(ref _name, value); }
        public int Threshold { get => _threshold; set => Set(ref _threshold, value); }
    }
}
