﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Drums
{
    public class DrumsViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Drum> _drums = new List<Drum>();
        private List<DrumModel> _drumModels = new List<DrumModel>();

        public DrumsViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            
            

            _drums = _databaseService.GetDrums();
            _drumModels = _databaseService.GetDrumModels();

            DrumModels = CollectionViewSource.GetDefaultView(_drumModels);
            Drums = CollectionViewSource.GetDefaultView(_drums);

            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (e.Value is Drum drum)
            {
                Application.Current.Dispatcher.Invoke(() => Drums.Refresh());
            }
            else if (e.Value is DrumModel drumModel)
            {
                Application.Current.Dispatcher.Invoke(() => DrumModels.Refresh());
            }
        }

        private void LoadCommands()
        {
            AddDrumCommand = new UiCommand(AddDrum);
            AddDrumModelCommand = new UiCommand(AddDrumModel);
            EditDrumCommand = new UiCommand(EditDrum);
            EditDrumModelCommand = new UiCommand(EditDrumModel);
            DeleteDrumCommand = new UiCommand(DeleteDrum);
            DeleteDrumModelCommand = new UiCommand(DeleteDrumModel);
        }

        private void DeleteDrumModel(object obj)
        {
            if (!(obj is DrumModel drumModel))
                return;

            _databaseService.RemoveDrumModel(drumModel);
        }

        private void DeleteDrum(object obj)
        {
            if (!(obj is Drum drum))
                return;

            _databaseService.RemoveDrum(drum);
        }

        private void EditDrum(object obj)
        {
            if (!(obj is Drum drum))
                return;

            var drumWizardVm = new DrumWizardViewModel(_databaseService, drum);
            NavigationService.WindowManager.CreateDialog(drumWizardVm);
        }

        private void EditDrumModel(object obj)
        {
            if (!(obj is DrumModel drumModel))
                return;

            var drumModelWizardVm = new DrumModelWizardViewModel(_databaseService, drumModel);
            NavigationService.WindowManager.CreateDialog(drumModelWizardVm);
        }

        private void AddDrumModel(object obj)
        {
            var drumModelWizardVm = new DrumModelWizardViewModel(_databaseService);
            NavigationService.WindowManager.CreateDialog(drumModelWizardVm);
        }

        private void AddDrum(object obj)
        {
            var drumWizardVm = new DrumWizardViewModel(_databaseService);
            NavigationService.WindowManager.CreateDialog(drumWizardVm);
        }

        public UiCommand AddDrumModelCommand { get; private set; }
        public UiCommand AddDrumCommand { get; private set; }
        public UiCommand EditDrumCommand { get; private set; }
        public UiCommand EditDrumModelCommand { get; private set; }
        public UiCommand DeleteDrumCommand { get; private set; }
        public UiCommand DeleteDrumModelCommand { get; private set; }

        public ICollectionView DrumModels { get; }
        public ICollectionView Drums { get; }
    }
}
