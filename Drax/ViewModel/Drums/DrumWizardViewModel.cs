﻿using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.Drums
{
    public class DrumWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;                
        private Drum _drum;

        private string _serialNumber = String.Empty;
        private double _price;
        private Invoice _selectedInvoice;
        private DrumModel _selectedDrumModel;
        
        private List<Invoice> _invoices;
        private List<DrumModel> _drumModels;                     

        public DrumWizardViewModel(IDatabaseService databaseService, Drum drum = null)
        {
            _databaseService = databaseService;
            _invoices = _databaseService.GetInvoices();
            _drumModels = _databaseService.GetDrumModels();

            if (drum != null)
            {
                _drum = drum;
                
                Price = drum.Price;
                SerialNumber = drum.SerialNumber;
                SelectedInvoice = _invoices.FirstOrDefault(i => i.Id == drum.Invoice.Id);
                SelectedDrumModel = _drumModels.FirstOrDefault(d => d.Id == drum.Model.Id);
                SelectedInvoice = _invoices.FirstOrDefault(i => i.Id == drum.Invoice.Id);                
            }
           
            Invoices = CollectionViewSource.GetDefaultView(_invoices);
            DrumModels = CollectionViewSource.GetDefaultView(_drumModels);                                 
        }                

        public override bool CanOk(object obj)
        {
            if (_serialNumber.Length > 0 && _selectedDrumModel != null && _selectedInvoice != null)
                return true;

            return false;
        }

        public override bool Ok(object obj)
        {
            if (_drum == null)
            {
                _drum = new Drum
                {
                    Model = _selectedDrumModel,
                    SerialNumber = _serialNumber,
                    Invoice = _selectedInvoice,
                    Price = _price
                };
                if ((_drum.Id = _databaseService.AddDrum(_drum)) > -1)
                {
                    return true;
                }
                else
                {
                    _drum = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _drum.Model = _selectedDrumModel;
                _drum.SerialNumber = _serialNumber;
                _drum.Invoice = _selectedInvoice;
                _drum.Price = _price;
                if ((_drum.Id = _databaseService.EditDrum(_drum)) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }                       
     
        public ICollectionView Invoices { get; }
        public ICollectionView DrumModels { get; }        

        public string SerialNumber { get => _serialNumber; set => Set(ref _serialNumber, value); }                
        public double Price { get => _price; set => Set(ref _price, value); }
        
        public DrumModel SelectedDrumModel { get => _selectedDrumModel; set => Set(ref _selectedDrumModel, value); }
        public Invoice SelectedInvoice { get => _selectedInvoice; set => Set(ref _selectedInvoice, value); }
    }
}
