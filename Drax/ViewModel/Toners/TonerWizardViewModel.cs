﻿using Drax.Helpers;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.Toners
{
    public class TonerWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private Toner _toner;

        private string _serialNumber = String.Empty;
        private double _price;
        private Invoice _selectedInvoice;
        private TonerModel _selectedTonerModel;

        private List<Invoice> _invoices;
        private List<TonerModel> _tonerModels;

        public TonerWizardViewModel(IDatabaseService databaseService, Toner toner = null)
        {
            _databaseService = databaseService;
            _invoices = _databaseService.GetInvoices();
            _tonerModels = _databaseService.GetTonerModels();

            if (toner != null)
            {
                _toner = toner;                
                Price = toner.Price;
                SerialNumber = toner.SerialNumber;
                SelectedInvoice = _invoices.FirstOrDefault(i => i.Id == toner.Invoice.Id);
                SelectedTonerModel = _tonerModels.FirstOrDefault(d => d.Id == toner.Model.Id);
                SelectedInvoice = _invoices.FirstOrDefault(i => i.Id == toner.Invoice.Id);
            }

            Invoices = CollectionViewSource.GetDefaultView(_invoices);
            TonerModels = CollectionViewSource.GetDefaultView(_tonerModels);            
        }                

        public override bool CanOk(object obj)
        {
            if (_serialNumber.Length > 0 && _selectedTonerModel != null && _selectedInvoice != null)
                return true;

            return false;
        }

        public override bool Ok(object obj)
        {
            if (_toner == null)
            {
                _toner = new Toner
                {
                    Model = _selectedTonerModel,
                    SerialNumber = _serialNumber,
                    Invoice = _selectedInvoice,
                    Price = _price
                };
                if ((_toner.Id = _databaseService.AddToner(_toner)) > -1)
                {
                    return true;
                }
                else
                {
                    _toner = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _toner.Model = _selectedTonerModel;
                _toner.SerialNumber = _serialNumber;
                _toner.Invoice = _selectedInvoice;
                _toner.Price = _price;
                if ((_toner.Id = _databaseService.EditToner(_toner)) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }       

        public ICollectionView Invoices { get; }
        public ICollectionView TonerModels { get; }

        public string SerialNumber { get => _serialNumber; set => Set(ref _serialNumber, value); }
        public double Price { get => _price; set => Set(ref _price, value); }

        public TonerModel SelectedTonerModel { get => _selectedTonerModel; set => Set(ref _selectedTonerModel, value); }
        public Invoice SelectedInvoice { get => _selectedInvoice; set => Set(ref _selectedInvoice, value); }
    }
}
