﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Toners
{
    public class TonersViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Toner> _toners;
        private List<TonerModel> _tonerModels;

        public TonersViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            

            _toners = _databaseService.GetToners();
            _tonerModels = _databaseService.GetTonerModels();
            
            LoadCommands();
            Toners = CollectionViewSource.GetDefaultView(_toners);
            TonerModels = CollectionViewSource.GetDefaultView(_tonerModels);
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (e.Value is Toner toner)
            {
                Application.Current.Dispatcher.Invoke(() => Toners.Refresh());
            }
            else if (e.Value is TonerModel tonerModel)
            {
                Application.Current.Dispatcher.Invoke(() => TonerModels.Refresh());
            }
        }

        private void LoadCommands()
        {
            AddTonerCommand = new UiCommand(AddToner);
            EditTonerCommand = new UiCommand(EditToner);
            DeleteTonerCommand = new UiCommand(DeleteToner);
            AddTonerModelCommand = new UiCommand(AddTonerModel);
            EditTonerModelCommand = new UiCommand(EditTonerModel);
            DeleteTonerModelCommand = new UiCommand(DeleteTonerModel);
        }

        private void AddTonerModel(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new TonerModelWizardViewModel(_databaseService));
        }

        private void EditTonerModel(object obj)
        {
            if (!(obj is TonerModel tonerModel))
                return;
            NavigationService.WindowManager.CreateDialog(new TonerModelWizardViewModel(_databaseService, tonerModel));
        }

        private void DeleteTonerModel(object obj)
        {
            if (!(obj is TonerModel tonerModel))
                return;
            _databaseService.RemoveTonerModel(tonerModel);
        }

        private void DeleteToner(object obj)
        {
            if (!(obj is Toner toner))
                return;
            _databaseService.RemoveToner(toner);
        }

        private void EditToner(object obj)
        {
            if (!(obj is Toner toner))
                return;
            NavigationService.WindowManager.CreateDialog(new TonerWizardViewModel(_databaseService, toner));
        }

        private void AddToner(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new TonerWizardViewModel(_databaseService));
        }

        public UiCommand AddTonerCommand { get; set; }
        public UiCommand EditTonerCommand { get; set; }
        public UiCommand DeleteTonerCommand { get; set; }
        public UiCommand AddTonerModelCommand { get; set; }
        public UiCommand EditTonerModelCommand { get; set; }
        public UiCommand DeleteTonerModelCommand { get; set; }
        public ICollectionView Toners { get; }
        public ICollectionView TonerModels { get; }
    }
}
