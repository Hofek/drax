﻿using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.Toners
{
    public class TonerModelWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private TonerModel _tonerModel;
        private List<string> _colors = Enum.GetNames(typeof(TonerModel.Color)).ToList();

        private string _selectedColor;
        private string _name = String.Empty;
        private int _threshold;

        public TonerModelWizardViewModel(IDatabaseService databaseService, TonerModel tonerModel = null)
        {
            _databaseService = databaseService;
            
            if (tonerModel != null)
            {
                _tonerModel = tonerModel;
                Name = tonerModel.Name;
                Threshold = tonerModel.Threshold;
                SelectedColor = _colors.FirstOrDefault(c => c == Enum.GetName(typeof(TonerModel.Color), tonerModel.ColorType));
            }

            Colors = CollectionViewSource.GetDefaultView(_colors);           
        }              

        public override bool CanOk(object obj)
        {
            if (_name.Length > 0 && _selectedColor != null)
                return true;

            return false;
        }

        public override bool Ok(object obj)
        {
            if (_tonerModel == null)
            {
                _tonerModel = new TonerModel
                {
                    Name = _name,
                    ColorType = (TonerModel.Color)Enum.Parse(typeof(TonerModel.Color), _selectedColor),
                    Threshold = _threshold
                };
                if ((_tonerModel.Id = _databaseService.AddTonerModel(_tonerModel)) > -1)
                {
                    return true;
                }
                else
                {
                    _tonerModel = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _tonerModel.Name = _name;
                _tonerModel.ColorType = (TonerModel.Color)Enum.Parse(typeof(TonerModel.Color), _selectedColor);
                _tonerModel.Threshold = _threshold;

                if ((_tonerModel.Id = _databaseService.EditTonerModel(_tonerModel)) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }
        
        public string Name { get => _name; set => Set(ref _name, value); }
        public string SelectedColor { get => _selectedColor; set => Set(ref _selectedColor, value); }
        public ICollectionView Colors { get; }
        public int Threshold { get => _threshold; set => Set(ref _threshold, value); }
    }
}
