﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Invoices
{
    public class InvoicesViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Invoice> _invoices;

        public InvoicesViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            
            _invoices = _databaseService.GetInvoices();
            Invoices = CollectionViewSource.GetDefaultView(_invoices);
            LoadCommands();
        }        

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (!(e.Value is Invoice invoice))
                return;

            Application.Current.Dispatcher.Invoke(() => Invoices.Refresh());
        }

        private void LoadCommands()
        {
            AddInvoiceCommand = new UiCommand(AddInvoice);
            EditInvoiceCommand = new UiCommand(EditInvoice);
            DeleteInvoiceCommand = new UiCommand(DeleteInvoice);
        }

        private void DeleteInvoice(object obj)
        {
            if (!(obj is Invoice invoice))
                return;

            _databaseService.RemoveInvoice(invoice);
        }

        private void EditInvoice(object obj)
        {
            if (!(obj is Invoice invoice))
                return;
            NavigationService.WindowManager.CreateDialog(new InvoiceWizardViewModel(_databaseService, invoice));
        }

        private void AddInvoice(object obj)
        {           
            NavigationService.WindowManager.CreateDialog(new InvoiceWizardViewModel(_databaseService));
        }

        public UiCommand AddInvoiceCommand { get; set; }
        public UiCommand DeleteInvoiceCommand { get; set; }
        public UiCommand EditInvoiceCommand { get; set; }
        public ICollectionView Invoices { get; }
    }
}
