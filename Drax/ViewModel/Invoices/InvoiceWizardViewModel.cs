﻿using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.Invoices
{
    public class InvoiceWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Supplier> _suppliers;     
        private Invoice _invoice;

        private string _number = String.Empty;
        private double _price;
        private DateTime _date;

        private Supplier _selectedSupplier;

        public InvoiceWizardViewModel(IDatabaseService databaseService, Invoice invoice = null)
        {
            _databaseService = databaseService;
            
            _suppliers = _databaseService.GetSuppliers();
            Suppliers = CollectionViewSource.GetDefaultView(_suppliers);
            if (invoice != null)
            {
                _invoice = invoice;
                Number = invoice.Number;
                Price = invoice.Price;
                Date = invoice.Date;
                SelectedSupplier = _suppliers.FirstOrDefault(s => s.Id == invoice.Supplier.Id);
            }            
        }               

        public override bool CanOk(object obj)
        {
            if (_number.Length > 0)
                return true;

            return false;
        }

        public override bool Ok(object obj)
        {
            if (_invoice == null)
            {
                _invoice = new Invoice
                {
                    Number = _number,
                    Date = _date,
                    Price = Price,
                    Supplier = SelectedSupplier
                };
                if ((_invoice.Id = _databaseService.AddInvoice(_invoice)) > -1)
                {
                    return true;
                }
                else
                {
                    _invoice = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _invoice.Number = _number;
                _invoice.Date = _date;
                _invoice.Price = Price;
                _invoice.Supplier = SelectedSupplier;                

                if ((_invoice.Id = _databaseService.EditInvoice(_invoice)) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }       

        public string Number { get => _number; set => Set(ref _number, value); }
        public double Price { get => _price; set => Set(ref _price, value); }
        public Supplier SelectedSupplier { get => _selectedSupplier; set => Set (ref _selectedSupplier, value); }
        public DateTime Date { get => _date; set => Set(ref _date, value); }
        public ICollectionView Suppliers { get; }
    }
}
