﻿using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;

namespace Drax.ViewModel.Places
{
    public class PlaceWizardViewModel : OkCancelViewModelBase
    {
        private IDatabaseService _dbService;
        
        private Place _place;        
        private string _name = String.Empty;
        private string _room = String.Empty;
        private int _floor;
        private string _description;

        public PlaceWizardViewModel(IDatabaseService dbService, Place place = null)
        {
            _dbService = dbService;
            
            if (place != null)
            {
                _place = place;
                _name = place.Name;
                _room = place.Room;
                _floor = place.Floor;
                _description = place.Description;                
            }            
        }                

        public override bool CanOk(object obj)
        {
            if (_name.Length > 0 && _room.Length > 0)
                return true;
            return false;
        }

        public override bool Ok(object obj)
        {
            if (_place == null)
            {
                _place = new Place
                {
                    Name = _name,
                    Floor = _floor,
                    Room = _room,
                    Description = _description                    
                };


                if ((_place.Id = _dbService.AddPlace(_place)) > -1)
                    return true;
                else
                {
                    _place = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {
                _place.Name = Name;
                _place.Description = _description;
                _place.Floor = _floor;
                _place.Room = _room;

                if (_dbService.EditPlace(_place) > -1)
                    return true;
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się zedytować wpisu!");
                    return false;
                }
                    
            }
                
        }        

        public string Name { get => _name; set => Set(ref _name, value); }
        public string Room { get => _room; set => Set(ref _room, value); }
        public int Floor { get => _floor; set => Set(ref _floor, value); }
        public string Description { get => _description; set => Set(ref _description, value); }
    }
}
