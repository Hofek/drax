﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Places
{
    public class PlacesViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Place> _places;

        public PlacesViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            

            _places = _databaseService.GetPlaces();
            Places = CollectionViewSource.GetDefaultView(_places);

            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (!(e.Value is Place place))
                return;

            Application.Current.Dispatcher.Invoke(() => Places.Refresh());
        }

        private void LoadCommands()
        {
            AddPlaceCommand = new UiCommand(AddPlace);
            EditPlaceCommand = new UiCommand(EditPlace);
            DeletePlaceCommand = new UiCommand(DeletePlace);
        }

        private void DeletePlace(object obj)
        {
            if (!(obj is Place place))
                return;

            _databaseService.RemovePlace(place);
        }

        private void EditPlace(object obj)
        {
            if (!(obj is Place place))
                return;

            NavigationService.WindowManager.CreateDialog(new PlaceWizardViewModel(_databaseService, place));
        }

        private void AddPlace(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new PlaceWizardViewModel(_databaseService));
        }

        public UiCommand AddPlaceCommand { get; set; }
        public UiCommand EditPlaceCommand { get; set; }
        public UiCommand DeletePlaceCommand { get; set; }

        public ICollectionView Places { get; }
    }
}
