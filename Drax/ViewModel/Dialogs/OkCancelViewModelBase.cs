﻿using Drax.Helpers;
using System.Windows.Input;

namespace Drax.ViewModel.Dialogs
{
    public class OkCancelViewModelBase : DialogViewModelBase
    {
        public bool OkCancelButtonsActivateViaKeyboard { get; set; } = true;

        /// <summary>
        /// OkCancelViewModelBase will provide basic support for Ok/Cancel concept dialog
        /// </summary>        
        /// <param name="okButtonContent">Ok button content</param>
        /// <param name="cancelButtonContent">Cancel button content</param>
        public OkCancelViewModelBase(string okButtonContent = "Ok", string cancelButtonContent = "Anuluj")
        {
            LoadCommands();

            OkButtonContent = okButtonContent;
            CancelButtonContent = cancelButtonContent;
        }

        private void LoadCommands()
        {
            OkCommand = new UiCommand((obj) =>
            {
                if (Ok(obj))
                    base.DialogConfirm();
            }, CanOk);

            CancelCommand = new UiCommand((obj) =>
            {
                Cancel(obj);
                base.DialogClose();
            }, CanCancel);
        }

        public virtual bool Ok(object obj = null) { return true; }
        public virtual bool CanOk(object obj) { return IsModified; }
        public virtual bool CanCancel(object obj) { return true; }
        public virtual void Cancel(object obj) { }

        public ICommand CancelCommand { get; protected set; }
        public ICommand OkCommand { get; protected set; }

        public string OkButtonContent { get; set; }
        public string CancelButtonContent { get; set; }
    }
}
