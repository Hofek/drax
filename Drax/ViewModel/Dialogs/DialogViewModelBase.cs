﻿using Drax.Helpers;
using Drax.Services;

namespace Drax.ViewModel.Dialogs
{
    public abstract class DialogViewModelBase : WindowViewModelBase
    {        
        public DialogViewModelBase()
        {            
            CloseCommand = new UiCommand(DialogClose);
        }

        public bool DialogResult { get; private set; }
        public void DialogConfirm()
        {
            DialogResult = true;
            NavigationService.WindowManager.Close(this);
        }

        public void DialogClose(object obj = null)
        {
            DialogResult = false;
            NavigationService.WindowManager.Close(this);
        }        
    }
}
