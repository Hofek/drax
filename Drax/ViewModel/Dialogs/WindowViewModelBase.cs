﻿using Drax.Helpers;
using Drax.Services;

namespace Drax.ViewModel.Dialogs
{
    public abstract class WindowViewModelBase : ModifyableViewModelBase
    {
        public ViewModelBase ContentViewModel { get; }
        public WindowViewModelBase()
        {
            ContentViewModel = this;
            CloseCommand = new UiCommand((obj) => NavigationService.WindowManager.Close(this));
        }
        public UiCommand CloseCommand { get; protected set; }
    }
}
