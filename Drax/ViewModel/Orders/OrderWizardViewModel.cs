﻿using Drax.Helpers;
using Drax.Services;
using Drax.ViewModel.Dialogs;
using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Drax.ViewModel.Orders
{
    public class OrderWizardViewModel : OkCancelViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Drum> _drums;
        private List<Toner> _toners;
        private List<Drum> _orderedDrums;
        private List<Toner> _orderedToners;
        private List<Person> _people;
        private Order _order;

        private Drum _selectedDrum;
        private Toner _selectedToner;

        private Person _selectedPerson;

        public OrderWizardViewModel(IDatabaseService databaseService, Order order = null)
        {
            _databaseService = databaseService;
            
            LoadCommands();

            _drums = _databaseService.GetDrums().Where(d => d.Order == null).ToList();
            _toners = _databaseService.GetToners().Where(t => t.Order == null).ToList();
            _people = _databaseService.GetPeople();

            if (order != null)
            {
                _order = order;
                _selectedPerson = _people.FirstOrDefault(p => p.Id == order.Client.Id);
                _orderedDrums = new List<Drum>(order.Drums);
                _orderedToners = new List<Toner>(order.Toners);
            }
            else
            {
                _orderedDrums = new List<Drum>();
                _orderedToners = new List<Toner>();
            }

            People = CollectionViewSource.GetDefaultView(_people);
            Drums = CollectionViewSource.GetDefaultView(_drums);
            OrderedDrums = CollectionViewSource.GetDefaultView(_orderedDrums);
            Toners = CollectionViewSource.GetDefaultView(_toners);
            OrderedToners = CollectionViewSource.GetDefaultView(_orderedToners);
        }

        private void LoadCommands()
        {
            AddOrderedDrumCommand = new UiCommand(AddOrderedDrum, CanAddOrderedDrum);
            AddOrderedTonerCommand = new UiCommand(AddOrderedToner, CanAddOrderedToner);
            DeleteOrderedDrumCommand = new UiCommand(DeleteOrderedDrum);
            DeleteOrderedTonerCommand = new UiCommand(DeleteOrderedToner);            
        }

        private void DeleteOrderedToner(object obj)
        {
            if (!(obj is Toner toner))
                return;

            _orderedToners.Remove(toner);
            OrderedToners.Refresh();
        }

        private void DeleteOrderedDrum(object obj)
        {
            if (!(obj is Drum drum))
                return;

            _orderedDrums.Remove(drum);
            OrderedDrums.Refresh();
        }        

        public override bool CanOk(object obj)
        {
            if (_selectedPerson != null && (_orderedDrums.Count>0 || _orderedToners.Count>0))
                return true;
            return false;
        }

        public override bool Ok(object obj)
        {
            if (_order == null)
            {
                _order = new Order
                {
                    Drums = _orderedDrums,
                    Toners = _orderedToners,
                    Client = _selectedPerson,
                    Department = _selectedPerson.Department,
                    Date = DateTime.Now
                };
                if ((_order.Id = _databaseService.AddOrder(_order)) > -1)
                {
                    return true;
                }
                else
                {
                    _order = null;
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
            else
            {                
                _order.Drums = _orderedDrums;
                _order.Toners = _orderedToners;
                _order.Client = _selectedPerson;
                _order.Department = _selectedPerson.Department;
                _order.Drums = _orderedDrums;
                _order.Toners = _orderedToners;

                if (_databaseService.EditOrder(_order) > -1)
                {
                    return true;
                }
                else
                {
                    NavigationService.WindowManager.CreateMessageBox("Nie udało się stworzyć wpisu!");
                    return false;
                }
                    
            }
        }

        private bool CanAddOrderedToner(object obj)
        {
            if (_selectedToner != null && _orderedToners.FirstOrDefault(t => t.Id == _selectedToner.Id) == null)
                return true;
            return false;
        }

        private void AddOrderedToner(object obj)
        {
            _orderedToners.Add(_selectedToner);
            OrderedToners.Refresh();
            SelectedToner = null;
        }

        private void AddOrderedDrum(object obj)
        {
            _orderedDrums.Add(_selectedDrum);
            OrderedDrums.Refresh();
            SelectedDrum = null;
        }

        private bool CanAddOrderedDrum(object obj)
        {
            if (_selectedDrum != null && _orderedDrums.FirstOrDefault(d => d.Id == _selectedDrum.Id) == null)
                return true;
            return false;
        }

        public UiCommand AddOrderedDrumCommand { get; set; }
        public UiCommand AddOrderedTonerCommand { get; set; }
        public UiCommand DeleteOrderedDrumCommand { get; set; }
        public UiCommand DeleteOrderedTonerCommand { get; set; }        

        public Person SelectedPerson { get => _selectedPerson; set => Set(ref _selectedPerson, value); }
        public ICollectionView Drums { get; }
        public ICollectionView Toners { get; }
        public ICollectionView OrderedDrums { get; }
        public ICollectionView OrderedToners { get; }   
        public ICollectionView People { get; }
        public Toner SelectedToner { get => _selectedToner; set => Set(ref _selectedToner, value); }
        public Drum SelectedDrum { get => _selectedDrum; set => Set(ref _selectedDrum, value); }
    }
}
