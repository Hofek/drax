﻿using Drax.Helpers;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Drax.ViewModel.Orders
{
    public class OrdersViewModel : ViewModelBase
    {
        private readonly IDatabaseService _databaseService;
        
        private List<Order> _orders;

        public OrdersViewModel(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
            _databaseService.DataChanged += DatabaseService_DataChanged;
            

            _orders = _databaseService.GetOrders();
            Orders = CollectionViewSource.GetDefaultView(_orders);

            LoadCommands();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_databaseService != null)
                _databaseService.DataChanged -= DatabaseService_DataChanged;
        }

        private void DatabaseService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (!(e.Value is Order order))
                return;

            Application.Current.Dispatcher.Invoke(() => Orders.Refresh());
        }

        private void LoadCommands()
        {
            AddOrderCommand = new UiCommand(AddOrder);
            EditOrderCommand = new UiCommand(EditOrder);
            DeleteOrderCommand = new UiCommand(DeleteOrder);
        }

        private void DeleteOrder(object obj)
        {
            if (!(obj is Order order))
                return;

            _databaseService.RemoveOrder(order);
        }

        private void EditOrder(object obj)
        {
            if (!(obj is Order order))
                return;

            NavigationService.WindowManager.CreateDialog(new OrderWizardViewModel(_databaseService, order));
        }

        private void AddOrder(object obj)
        {
            NavigationService.WindowManager.CreateDialog(new OrderWizardViewModel(_databaseService));
        }

        public UiCommand AddOrderCommand { get; set; }
        public UiCommand EditOrderCommand { get; set; }
        public UiCommand DeleteOrderCommand { get; set; }

        public ICollectionView Orders { get; }
    }
}
