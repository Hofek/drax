﻿using Drax.Model;
using Drax.Services;
using DraxCommon.Model;
using DraxCommon.Services;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using static DraxCommon.Model.Enums;

namespace Drax.Communication
{
    public class Client : CommunicationBase
    {
        private ConcurrentQueue<TcpMessage> _receivedRequestedMessages = new ConcurrentQueue<TcpMessage>();
        private SemaphoreSlim _requestSemaphore = new SemaphoreSlim(0);

        public Client()
        {
            MessageReceived += Client_MessageReceived;
        }

        private void Client_MessageReceived(object sender, DraxCommon.Model.Args.SingleValueEventArgs<TcpMessage> e)
        {
            if (e.Item.MessageType == Enums.MessageType.Response)
            {
                _receivedRequestedMessages.Enqueue(e.Item);
                _requestSemaphore.Release();
            }
        }

        public T Request<T>(MessageType messageType, string methodName, object parameter = null)
        {
            Send(new TcpMessage(messageType, methodName, parameter));
            _requestSemaphore.Wait();
            _receivedRequestedMessages.TryDequeue(out var result);
            if (messageType == MessageType.Add && (int)result.Value == -1)
                NavigationService.WindowManager.CreateMessageBox("Nie można dodać elementu!");
            else if (messageType == MessageType.Edit && (int)result.Value == -1)
                NavigationService.WindowManager.CreateMessageBox("Nie można zedytować elementu!");
            else if (messageType == MessageType.Remove && (int)result.Value == -1)
                NavigationService.WindowManager.CreateMessageBox("Nie można usunąć elementu!");            

            return (T)result.Value;
        }

        public async Task Connect()
        {            
            while (true)
            {
                try
                {
                    Client = new TcpClient();
                
                    if (!IPAddress.TryParse(Config.Instance.Hostname, out var ip))
                    {
                        await Task.Delay(3000);
                        Debug.WriteLine("Could not parse ip");
                        continue;
                    }
                    var port = Config.Instance.Port;
                    await Client.ConnectAsync(ip, port).ConfigureAwait(false);

                    StartCommunicationThreads();
                    Console.WriteLine($"[{Client.Client.RemoteEndPoint}] Connection established!");                    
                }
                catch (Exception ex)
                {
                    if (ex is NullReferenceException)
                    {
                        Debug.WriteLine("Terminating client init");
                        break;
                    }
                        

                    await Task.Delay(3000);
                    Debug.WriteLine("Could not connect to service. Reconnecting...");
                    continue;
                }                
                Debug.WriteLine("Connected to service!");                
                break;
            }
            return;
        }
    }
}
