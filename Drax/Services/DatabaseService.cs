﻿using Drax.Communication;
using Drax.Model;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Drax.Services
{
    public class DatabaseService : IDatabaseService
    {
        public event EventHandler<DataChangedEventArgs> DataChanged;
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        private Client _client = new Client();
        private int _disposed = 0;

        private Lazy<List<Drum>> _drums;
        private Lazy<List<Department>> _departments;
        private Lazy<List<DrumModel>> _drumModels;
        private Lazy<List<Invoice>> _invoices;
        private Lazy<List<Order>> _orders;
        private Lazy<List<Person>> _people;
        private Lazy<List<Place>> _places;
        private Lazy<List<PrinterModel>> _printerModels;
        private Lazy<List<Printer>> _printers;
        private Lazy<List<Supplier>> _suppliers;
        private Lazy<List<TonerModel>> _tonerModels;
        private Lazy<List<Toner>> _toners;
        private Lazy<List<PrinterStatus>> _printerStatus;

        public DatabaseService()
        {
            _client.MessageReceived += Client_MessageReceived;
            _client.Disconnected += Client_Disconnected;            
            _ = Init();

            _drums = new Lazy<List<Drum>>(() => 
            { 
                var list = _client.Request<List<Drum>>(Enums.MessageType.Query, nameof(GetDrums));
                foreach(var element in list)
                {
                    element.Model = _drumModels.Value.FirstOrDefault(m => m.Id == element.Model?.Id);
                    element.Invoice = _invoices.Value.FirstOrDefault(i => i.Id == element.Invoice?.Id);
                    if (_drums.IsValueCreated)
                        element.Order = _orders.Value.FirstOrDefault(o => o.Id == element.Order?.Id);
                }
                return list;
            });

            _departments = new Lazy<List<Department>>(() => 
            { 
                var list = _client.Request<List<Department>>(Enums.MessageType.Query, nameof(GetDepartments));
                foreach (var element in list)                
                    element.Supervisor = _people.Value.FirstOrDefault(p => p.Id == element.Supervisor?.Id);                    
                
                return list; 
            });

            _drumModels = new Lazy<List<DrumModel>>(() => 
            { 
                return _client.Request<List<DrumModel>>(Enums.MessageType.Query, nameof(GetDrumModels));                                  
            });

            _invoices = new Lazy<List<Invoice>>(() => 
            { 
                var list = _client.Request<List<Invoice>>(Enums.MessageType.Query, nameof(GetInvoices));
                foreach (var element in list)
                    element.Supplier = _suppliers.Value.FirstOrDefault(s => s.Id == element.Supplier?.Id);

                return list;
            });

            _orders = new Lazy<List<Order>>(() => 
            { 
                var list = _client.Request<List<Order>>(Enums.MessageType.Query, nameof(GetOrders));
                foreach(var element in list)
                {
                    element.Department = _departments.Value.FirstOrDefault(d => d.Id == element.Department?.Id);
                    element.Client = _people.Value.FirstOrDefault(p => p.Id == element.Client?.Id);

                    if (_toners.IsValueCreated)
                    {
                        element.Toners = new List<Toner>();
                        foreach (var toner in _toners.Value)
                            if (toner.Order?.Id == element.Id)
                                element.Toners.Add(toner);
                    }
                    
                    if (_drums.IsValueCreated)
                    {
                        element.Drums = new List<Drum>();
                        foreach (var drum in _drums.Value)
                            if (drum.Order?.Id == element.Id)
                                element.Drums.Add(drum);
                    }                    
                }
                return list;
            });

            _people = new Lazy<List<Person>>(() => 
            { 
                var list = _client.Request<List<Person>>(Enums.MessageType.Query, nameof(GetPeople));
                foreach (var element in list)
                    if (_departments.IsValueCreated)
                    {
                        element.Department = _departments.Value.FirstOrDefault(d => d.Id == element.Department?.Id);
                        foreach (var person in _people.Value)
                            person.Department = _departments.Value.FirstOrDefault(d => d.Id == person.Department?.Id);
                    }
                        
                return list;
            });

            _places = new Lazy<List<Place>>(() => 
            { 
                return _client.Request<List<Place>>(Enums.MessageType.Query, nameof(GetPlaces));                
            });

            _printerStatus = new Lazy<List<PrinterStatus>>(() =>
            {
                return _client.Request<List<PrinterStatus>>(Enums.MessageType.Query, nameof(GetPrinterStatus));
            });

            _printerModels = new Lazy<List<PrinterModel>>(() => 
            { 
                var list = _client.Request<List<PrinterModel>>(Enums.MessageType.Query, nameof(GetPrinterModels));
                foreach (var element in list)
                {
                    element.Drums = new List<DrumModel>();
                    foreach (var drumModel in _client.Request<List<DrumModel>>(Enums.MessageType.Query, nameof(GetCompatibleDrums)))
                        if (_drumModels.Value.FirstOrDefault(m => m.Id == drumModel.Id) != null)
                            element.Drums.Add(_drumModels.Value.FirstOrDefault(m => m.Id == drumModel.Id));

                    element.Toners = new List<TonerModel>();
                    foreach (var tonerModel in _client.Request<List<TonerModel>>(Enums.MessageType.Query, nameof(GetCompatibleToners)))
                        if (_tonerModels.Value.FirstOrDefault(m => m.Id == tonerModel.Id) != null)
                            element.Toners.Add(_tonerModels.Value.FirstOrDefault(m => m.Id == tonerModel.Id));
                }
                    
                return list;
            });

            _printers = new Lazy<List<Printer>>(() => 
            { 
                var list = _client.Request<List<Printer>>(Enums.MessageType.Query, nameof(GetPrinters));
                foreach (var element in list)
                {
                    element.Model = _printerModels.Value.FirstOrDefault(p => p.Id == element.Model?.Id);
                    element.Place = _places.Value.FirstOrDefault(p => p.Id == element.Place?.Id);
                }
                return list;
            });

            _suppliers = new Lazy<List<Supplier>>(() => 
            {                 
                return _client.Request<List<Supplier>>(Enums.MessageType.Query, nameof(GetSuppliers));
            });

            _tonerModels = new Lazy<List<TonerModel>>(() => 
            {                
                return _client.Request<List<TonerModel>>(Enums.MessageType.Query, nameof(GetTonerModels));
            });

            _toners = new Lazy<List<Toner>>(() => 
            { 
                var list = _client.Request<List<Toner>>(Enums.MessageType.Query, nameof(GetToners));
                foreach (var element in list)
                {
                    element.Model = _tonerModels.Value.FirstOrDefault(m => m.Id == element.Model?.Id);
                    element.Invoice = _invoices.Value.FirstOrDefault(i => i.Id == element.Invoice?.Id);
                    if (_orders.IsValueCreated)
                        element.Order = _orders.Value.FirstOrDefault(o => o.Id == element.Order?.Id);
                }
                return list;
            });
        }

        private void Client_Disconnected(object sender, EventArgs e)
        {
            Disconnected?.Invoke(this, EventArgs.Empty);

            _client.MessageReceived -= Client_MessageReceived;
            _client.Disconnected -= Client_Disconnected;           

            if (_disposed != default)
                return;

            _client = new Client();
            _client.MessageReceived += Client_MessageReceived;
            _client.Disconnected += Client_Disconnected;
            _ = Init();
        }

        private void Client_MessageReceived(object sender, SingleValueEventArgs<TcpMessage> e)
        {
            switch (e.Item.MessageType)
            {
                case Enums.MessageType.Add:
                    if (e.Item.Value is Drum)
                    {
                        ((Drum)e.Item.Value).Invoice = _invoices.Value.FirstOrDefault(i => i.Id == ((Drum)e.Item.Value).Invoice?.Id);
                        ((Drum)e.Item.Value).Model = _drumModels.Value.FirstOrDefault(m => m.Id == ((Drum)e.Item.Value).Model?.Id);
                        ((Drum)e.Item.Value).Order = _orders.Value.FirstOrDefault(o => o.Id == ((Drum)e.Item.Value).Order?.Id);
                        _drums.Value.Add((Drum)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Department)
                    {
                        ((Department)e.Item.Value).Supervisor = _people.Value.FirstOrDefault(p => p.Id == ((Department)e.Item.Value).Supervisor?.Id);
                        _departments.Value.Add((Department)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is DrumModel)
                        _drumModels.Value.Add((DrumModel)e.Item.Value);
                    else if (e.Item.Value is Invoice)
                    {
                        ((Invoice)e.Item.Value).Supplier = _suppliers.Value.FirstOrDefault(s => s.Id == ((Invoice)e.Item.Value).Supplier?.Id);
                        _invoices.Value.Add((Invoice)e.Item.Value);
                    }                        
                    else if (e.Item.Value is Order)
                    {
                        ((Order)e.Item.Value).Department = _departments.Value.FirstOrDefault(d => d.Id == ((Order)e.Item.Value).Department?.Id);
                        ((Order)e.Item.Value).Client = _people.Value.FirstOrDefault(p => p.Id == ((Order)e.Item.Value).Client?.Id);

                        ((Order)e.Item.Value).Toners = new List<Toner>();
                        foreach (var toner in _toners.Value)
                            if (toner.Order?.Id == ((Order)e.Item.Value).Id)
                                ((Order)e.Item.Value).Toners.Add(toner);

                        ((Order)e.Item.Value).Drums = new List<Drum>();
                        foreach (var drum in _drums.Value)
                            if (drum.Order?.Id == ((Order)e.Item.Value).Id)
                                ((Order)e.Item.Value).Drums.Add(drum);

                        _orders.Value.Add((Order)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Person)
                    {
                        ((Person)e.Item.Value).Department = _departments.Value.FirstOrDefault(d => d.Id == ((Person)e.Item.Value).Department?.Id);
                        _people.Value.Add((Person)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Place)
                        _places.Value.Add((Place)e.Item.Value);
                    else if (e.Item.Value is PrinterModel)
                    {
                        ((PrinterModel)e.Item.Value).Drums = new List<DrumModel>();
                        foreach (var drumModel in ((PrinterModel)e.Item.Value).Drums)
                            if (_drumModels.Value.FirstOrDefault(m => m.Id == drumModel.Id) != null)
                                ((PrinterModel)e.Item.Value).Drums.Add(_drumModels.Value.FirstOrDefault(m => m.Id == drumModel.Id));

                        ((PrinterModel)e.Item.Value).Toners = new List<TonerModel>();
                        foreach (var tonerModel in ((PrinterModel)e.Item.Value).Toners)
                            if (_tonerModels.Value.FirstOrDefault(m => m.Id == tonerModel.Id) != null)
                                ((PrinterModel)e.Item.Value).Toners.Add(_tonerModels.Value.FirstOrDefault(m => m.Id == tonerModel.Id));
                        
                        _printerModels.Value.Add((PrinterModel)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Printer)
                    {
                        ((Printer)e.Item.Value).Model = _printerModels.Value.FirstOrDefault(m => m.Id == ((Printer)e.Item.Value).Model?.Id);
                        ((Printer)e.Item.Value).Place = _places.Value.FirstOrDefault(m => m.Id == ((Printer)e.Item.Value).Place?.Id);
                        _printers.Value.Add((Printer)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Supplier)
                        _suppliers.Value.Add((Supplier)e.Item.Value);
                    else if (e.Item.Value is TonerModel)
                        _tonerModels.Value.Add((TonerModel)e.Item.Value);
                    else if (e.Item.Value is Toner)
                    {
                        ((Toner)e.Item.Value).Invoice = _invoices.Value.FirstOrDefault(i => i.Id == ((Toner)e.Item.Value).Invoice?.Id);
                        ((Toner)e.Item.Value).Order = _orders.Value.FirstOrDefault(i => i.Id == ((Toner)e.Item.Value).Order?.Id);
                        ((Toner)e.Item.Value).Model = _tonerModels.Value.FirstOrDefault(i => i.Id == ((Toner)e.Item.Value).Model?.Id);
                        _toners.Value.Add((Toner)e.Item.Value);
                    }
                        

                    DataChanged?.Invoke(this, new DataChangedEventArgs(e.Item.Value, DataChangedEventArgs.ChangeType.Add));

                    break;

                case Enums.MessageType.Edit:
                    if (e.Item.Value is Drum)
                    {
                        ((Drum)e.Item.Value).Invoice = _invoices.Value.FirstOrDefault(i => i.Id == ((Drum)e.Item.Value).Invoice?.Id);
                        ((Drum)e.Item.Value).Model = _drumModels.Value.FirstOrDefault(m => m.Id == ((Drum)e.Item.Value).Model?.Id);
                        ((Drum)e.Item.Value).Order = _orders.Value.FirstOrDefault(o => o.Id == ((Drum)e.Item.Value).Order?.Id);
                        _drums.Value[_drums.Value.FindIndex(d => d.Id == ((Drum)e.Item.Value).Id)] = ((Drum)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Department)
                    {
                        ((Department)e.Item.Value).Supervisor = _people.Value.FirstOrDefault(p => p.Id == ((Department)e.Item.Value).Supervisor?.Id);
                        _departments.Value[_departments.Value.FindIndex(d => d.Id == ((Department)e.Item.Value).Id)] = ((Department)e.Item.Value);
                    }

                    else if (e.Item.Value is PrinterStatus)
                    {                        
                        if (_printers.Value.FirstOrDefault(p => p.Ip.ToString() == ((PrinterStatus)e.Item.Value).Printer.Ip.ToString()) != null)
                            _printers.Value.FirstOrDefault(p => p.Ip.ToString() == ((PrinterStatus)e.Item.Value).Printer.Ip.ToString()).Status = ((PrinterStatus)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is DrumModel)                    
                        _drumModels.Value[_drumModels.Value.FindIndex(m => m.Id == ((DrumModel)e.Item.Value).Id)] = ((DrumModel)e.Item.Value);
                    
                        
                    else if (e.Item.Value is Invoice)
                    {
                        ((Invoice)e.Item.Value).Supplier = _suppliers.Value.FirstOrDefault(s => s.Id == ((Invoice)e.Item.Value).Supplier?.Id);
                        _invoices.Value[_invoices.Value.FindIndex(d => d.Id == ((Invoice)e.Item.Value).Id)] = ((Invoice)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Order)
                    {
                        ((Order)e.Item.Value).Department = _departments.Value.FirstOrDefault(d => d.Id == ((Order)e.Item.Value).Department?.Id);
                        ((Order)e.Item.Value).Client = _people.Value.FirstOrDefault(p => p.Id == ((Order)e.Item.Value).Client?.Id);

                        var oldOrderIndex = _orders.Value.FindIndex(d => d.Id == ((Order)e.Item.Value).Id);                       

                        foreach (var toner in _toners.Value)
                            if ((_toners.Value.FirstOrDefault(t => t.Order?.Id == ((Order)e.Item.Value).Id) != null))
                                _toners.Value.FirstOrDefault(t => t.Order?.Id == ((Order)e.Item.Value).Id).Order = null;

                        foreach (var drum in _drums.Value)
                            if ((_drums.Value.FirstOrDefault(t => t.Order?.Id == ((Order)e.Item.Value).Id) != null))
                                _drums.Value.FirstOrDefault(t => t.Order?.Id == ((Order)e.Item.Value).Id).Order = null;

                        _orders.Value[oldOrderIndex] = ((Order)e.Item.Value);

                        foreach (var toner in _orders.Value[oldOrderIndex].Toners)                        
                            _toners.Value.FirstOrDefault(t => t.Id == toner.Id).Order = _orders.Value.FirstOrDefault(o => o.Id == ((Order)e.Item.Value).Id);                                                    
                        
                        foreach (var drum in _orders.Value[oldOrderIndex].Drums)                        
                            _drums.Value.FirstOrDefault(t => t.Id == drum.Id).Order = _orders.Value.FirstOrDefault(o => o.Id == ((Order)e.Item.Value).Id);

                        _orders.Value[oldOrderIndex].Toners = new List<Toner>();
                        foreach (var toner in _toners.Value)
                            if (toner.Order?.Id == _orders.Value[oldOrderIndex].Id)
                                _orders.Value[oldOrderIndex].Toners.Add(toner);

                        _orders.Value[oldOrderIndex].Drums = new List<Drum>();
                        foreach (var drum in _drums.Value)
                            if (drum.Order?.Id == _orders.Value[oldOrderIndex].Id)
                                _orders.Value[oldOrderIndex].Drums.Add(drum);

                    }
                        
                    else if (e.Item.Value is Person)
                    {
                        ((Person)e.Item.Value).Department = _departments.Value.FirstOrDefault(d => d.Id == ((Person)e.Item.Value).Department?.Id);
                        _people.Value[_people.Value.FindIndex(d => d.Id == ((Person)e.Item.Value).Id)] = ((Person)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Place)
                        _places.Value[_places.Value.FindIndex(d => d.Id == ((Place)e.Item.Value).Id)] = ((Place)e.Item.Value);

                    else if (e.Item.Value is PrinterModel)
                    {
                        ((PrinterModel)e.Item.Value).Drums = new List<DrumModel>();
                        foreach (var drumModel in ((PrinterModel)e.Item.Value).Drums)
                            if (_drumModels.Value.FirstOrDefault(m => m.Id == drumModel.Id) != null)
                                ((PrinterModel)e.Item.Value).Drums.Add(_drumModels.Value.FirstOrDefault(m => m.Id == drumModel.Id));

                        ((PrinterModel)e.Item.Value).Toners = new List<TonerModel>();
                        foreach (var tonerModel in ((PrinterModel)e.Item.Value).Toners)
                            if (_tonerModels.Value.FirstOrDefault(m => m.Id == tonerModel.Id) != null)
                                ((PrinterModel)e.Item.Value).Toners.Add(_tonerModels.Value.FirstOrDefault(m => m.Id == tonerModel.Id));

                        _printerModels.Value[_printerModels.Value.FindIndex(d => d.Id == ((PrinterModel)e.Item.Value).Id)] = ((PrinterModel)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Printer)
                    {
                        ((Printer)e.Item.Value).Model = _printerModels.Value.FirstOrDefault(m => m.Id == ((Printer)e.Item.Value).Model?.Id);
                        ((Printer)e.Item.Value).Place = _places.Value.FirstOrDefault(m => m.Id == ((Printer)e.Item.Value).Place?.Id);
                        _printers.Value[_printers.Value.FindIndex(d => d.Id == ((Printer)e.Item.Value).Id)] = ((Printer)e.Item.Value);
                    }
                        
                    else if (e.Item.Value is Supplier)                    
                        _suppliers.Value[_suppliers.Value.FindIndex(d => d.Id == ((Supplier)e.Item.Value).Id)] = ((Supplier)e.Item.Value);
                    
                        
                    else if (e.Item.Value is TonerModel)
                        _tonerModels.Value[_tonerModels.Value.FindIndex(d => d.Id == ((TonerModel)e.Item.Value).Id)] = ((TonerModel)e.Item.Value);
                    
                    else if (e.Item.Value is Toner)
                    {
                        ((Toner)e.Item.Value).Invoice = _invoices.Value.FirstOrDefault(i => i.Id == ((Toner)e.Item.Value).Invoice?.Id);
                        ((Toner)e.Item.Value).Order = _orders.Value.FirstOrDefault(i => i.Id == ((Toner)e.Item.Value).Order?.Id);
                        ((Toner)e.Item.Value).Model = _tonerModels.Value.FirstOrDefault(i => i.Id == ((Toner)e.Item.Value).Model?.Id);
                        _toners.Value[_toners.Value.FindIndex(d => d.Id == ((Toner)e.Item.Value).Id)] = ((Toner)e.Item.Value);
                    }
                        

                    DataChanged?.Invoke(this, new DataChangedEventArgs(e.Item.Value, DataChangedEventArgs.ChangeType.Edit));

                    break;

                case Enums.MessageType.Remove:
                    if (e.Item.Value is Drum)
                        _drums.Value.RemoveAll(d => d.Id == ((Drum)e.Item.Value).Id);
                    else if (e.Item.Value is Department)
                        _departments.Value.RemoveAll(d => d.Id == ((Department)e.Item.Value).Id);
                    else if (e.Item.Value is DrumModel)
                        _drumModels.Value.RemoveAll(d => d.Id == ((DrumModel)e.Item.Value).Id);
                    else if (e.Item.Value is Invoice)
                        _invoices.Value.RemoveAll(d => d.Id == ((Invoice)e.Item.Value).Id);
                    else if (e.Item.Value is Order)
                    {
                        foreach (var toner in _toners.Value)
                            if (toner.Order?.Id == ((Order)e.Item.Value).Id)
                                toner.Order = null;

                        foreach (var drum in _drums.Value)
                            if (drum.Order?.Id == ((Order)e.Item.Value).Id)
                                drum.Order = null;

                        _orders.Value.RemoveAll(d => d.Id == ((Order)e.Item.Value).Id);
                    }
                        
                    else if (e.Item.Value is Person)
                        _people.Value.RemoveAll(d => d.Id == ((Person)e.Item.Value).Id);
                    else if (e.Item.Value is Place)
                        _places.Value.RemoveAll(d => d.Id == ((Place)e.Item.Value).Id);
                    else if (e.Item.Value is PrinterModel)
                        _printerModels.Value.RemoveAll(d => d.Id == ((PrinterModel)e.Item.Value).Id);
                    else if (e.Item.Value is Printer)
                        _printers.Value.RemoveAll(d => d.Id == ((Printer)e.Item.Value).Id);
                    else if (e.Item.Value is Supplier)
                        _suppliers.Value.RemoveAll(d => d.Id == ((Supplier)e.Item.Value).Id);
                    else if (e.Item.Value is TonerModel)
                        _tonerModels.Value.RemoveAll(d => d.Id == ((TonerModel)e.Item.Value).Id);
                    else if (e.Item.Value is Toner)
                        _toners.Value.RemoveAll(d => d.Id == ((Toner)e.Item.Value).Id);
                    DataChanged?.Invoke(this, new DataChangedEventArgs(e.Item.Value, DataChangedEventArgs.ChangeType.Delete));
                    break;
            }           
        }

        private async Task Init()
        {
            await _client.Connect().ConfigureAwait(false);
            Connected?.Invoke(this, EventArgs.Empty);
        }

        public int AddDepartment(Department department)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, department);
        }

        public int AddDrum(Drum drum)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, drum);
        }

        public int AddDrumModel(DrumModel drumModel)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, drumModel);
        }

        public int AddInvoice(Invoice invoice)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, invoice);
        }

        public int AddOrder(Order order)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, order);
        }

        public int AddPerson(Person person)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, person);
        }

        public int AddPlace(Place place)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, place);
        }

        public int AddPrinter(Printer printer)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, printer);
        }

        public int AddPrinterModel(PrinterModel printerModel)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, printerModel);
        }

        public int AddSupplier(Supplier supplier)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, supplier);
        }

        public int AddToner(Toner toner)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, toner);
        }

        public int AddTonerModel(TonerModel tonerModel)
        {
            return _client.Request<int>(Enums.MessageType.Add, MethodBase.GetCurrentMethod().Name, tonerModel);
        }

        public void Dispose()
        {
            if (!(Interlocked.Exchange(ref _disposed, 1) == default(int)))
                return;

            Debug.WriteLine("Disposing DatabaseService");
            _client.MessageReceived -= Client_MessageReceived;
            _client.Disconnected -= Client_Disconnected;
            _client.Dispose();
            _client = null;
            Disconnected?.Invoke(this, EventArgs.Empty);
        }

        public int EditDepartment(Department department)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, department);
        }

        public int EditDrum(Drum drum)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, drum);
        }

        public int EditDrumModel(DrumModel drumModel)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, drumModel);
        }

        public int EditInvoice(Invoice invoice)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, invoice);
        }

        public int EditOrder(Order order)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, order);
        }

        public int EditPerson(Person person)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, person);
        }

        public int EditPlace(Place place)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, place);
        }

        public int EditPrinter(Printer printer)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, printer);
        }

        public int EditPrinterModel(PrinterModel printerModel)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, printerModel);
        }

        public int EditSupplier(Supplier supplier)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, supplier);
        }

        public int EditToner(Toner toner)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, toner);
        }

        public int EditTonerModel(TonerModel toner)
        {
            return _client.Request<int>(Enums.MessageType.Edit, MethodBase.GetCurrentMethod().Name, toner);
        }

        public List<Department> GetDepartments()
        {
            return _departments.Value;
        }

        public List<DrumModel> GetDrumModels()
        {
            return _drumModels.Value;
        }

        public List<Drum> GetDrums()
        {
            return _drums.Value;
        }

        public List<Invoice> GetInvoices()
        {
            return _invoices.Value;
        }

        public List<Order> GetOrders()
        {
            return _orders.Value;
        }

        public List<Person> GetPeople()
        {
            return _people.Value;
        }

        public List<Place> GetPlaces()
        {
            return _places.Value;
        }

        public List<PrinterModel> GetPrinterModels()
        {
            return _printerModels.Value;
        }

        public List<Printer> GetPrinters()
        {
            return _printers.Value;
        }

        public List<Supplier> GetSuppliers()
        {
            return _suppliers.Value;
        }

        public List<TonerModel> GetTonerModels()
        {
            return _tonerModels.Value;
        }

        public List<Toner> GetToners()
        {
            return _toners.Value;
        }

        public void RaiseDataChanged(object sender, DataChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public int RemoveDepartment(Department department)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, department);
        }

        public int RemoveDrum(Drum drum)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, drum);
        }

        public int RemoveDrumModel(DrumModel drumModel)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, drumModel);
        }

        public int RemoveInvoice(Invoice invoice)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, invoice);
        }

        public int RemoveOrder(Order order)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, order);
        }

        public int RemovePerson(Person person)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, person);
        }

        public int RemovePlace(Place place)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, place);
        }

        public int RemovePrinter(Printer printer)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, printer);
        }

        public int RemovePrinterModel(PrinterModel printerModel)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, printerModel);
        }

        public int RemoveSupplier(Supplier supplier)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, supplier);
        }

        public int RemoveToner(Toner toner)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, toner);
        }

        public int RemoveTonerModel(TonerModel tonerModel)
        {
            return _client.Request<int>(Enums.MessageType.Remove, MethodBase.GetCurrentMethod().Name, tonerModel);
        }

        public List<DrumModel> GetCompatibleDrums(int printerId)
        {
            var list = _client.Request<List<DrumModel>>(Enums.MessageType.Query, MethodBase.GetCurrentMethod().Name, printerId);
            List<DrumModel> resultList = new List<DrumModel>();
            foreach (var element in list)
                resultList.Add(_drumModels.Value.FirstOrDefault(m => m.Id == element.Id));
            
            return resultList;
        }

        public List<TonerModel> GetCompatibleToners(int tonerId)
        {
            var list = _client.Request<List<TonerModel>>(Enums.MessageType.Query, MethodBase.GetCurrentMethod().Name, tonerId);
            List<TonerModel> resultList = new List<TonerModel>();
            foreach (var element in list)
                resultList.Add(_tonerModels.Value.FirstOrDefault(m => m.Id == element.Id));

            return resultList;
        }

        public List<PrinterStatus> GetPrinterStatus()
        {
            return _printerStatus.Value;
        }
    }
}
