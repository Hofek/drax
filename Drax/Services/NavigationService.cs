﻿using Drax.Helpers;
using Drax.View.Dialogs;
using Drax.ViewModel.Dialogs;
using Drax.ViewModel.Messages;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Drax.Services
{
    public class NavigationService : INavigationService, IWindowManager
    {
        public static IWindowManager WindowManager { get; private set; } = new NavigationService();

        private readonly HashSet<Window> _windows = new HashSet<Window>();        
        public event EventHandler<SingleValueEventArgs<string>> MainViewChange;      

        public void RaiseMainViewChange(string obj)
        {
            MainViewChange?.Invoke(this, new SingleValueEventArgs<string>(obj));
        }        

        public bool? CreateDialog(object content)
        {
            if (!(content is ViewModelBase))
                return false;

            var _window = _windows.FirstOrDefault(p => p.DataContext.GetType() == content.GetType());
            if (_window != null)
            {
                _window.Activate();
                return true;
            }

            _window = new DraxWindow
            {                                
                SizeToContent = SizeToContent.WidthAndHeight
            };

            _window.DataContext = content;            
            _window.Closed += Window_Closed;
            _windows.Add(_window);
            _window.ShowDialog();
            if (_window.DataContext is DialogViewModelBase dialog)
                return dialog.DialogResult;

            return _window.DialogResult;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (sender is Window window && window.DataContext is ViewModelBase viewModel)
                _windows.Remove(_windows.FirstOrDefault(w => w.DataContext == viewModel));
        }

        public void Close(object content)
        {
            _windows.FirstOrDefault(p => p.DataContext == content)?.Close();
        }

        public void InitWindowManager(IWindowManager windowManager)
        {
            WindowManager = windowManager;
        }
        public bool? CreateMessageBox(string content)
        {
            return CreateDialog(new MessageViewModel(content));
        }        
    }
}
