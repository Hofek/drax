﻿using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Drax.Services
{
    public class LocalTestDatabaseService : IDatabaseService
    {
        private List<Department> _departments = new List<Department>()
        {
            new Department { Id=0, Name="Newsroom"},
            new Department { Id=1, Name="IT"},
            new Department { Id=2, Name="Kadry"},
            new Department { Id=3, Name="Archiwum"}
        };

        private List<Person> _people = new List<Person>
        {
            new Person { Id=0, Name="Julia", Surname="Kleszczonek"},
            new Person { Id=1, Name="Alex", Surname="Kasak"},
            new Person { Id=2, Name="Mateusz", Surname="Bruzdewicz"},
            new Person { Id=3, Name="Patryk", Surname="Zadurski"},
            new Person { Id=4, Name="Piotr", Surname="Hofbauer"},
        };

        private List<Supplier> _suppliers = new List<Supplier>
        {
            new Supplier { Id=0, Name="Jing Jang Bille", Email="JingJang@tonery.pl", Phone="123-456-789", Description="" }
        };

        private List<Place> _places = new List<Place>
        {
            new Place {Id=0, Name="2/6", Floor=2, Room="6", Description="Pokój IT"},
            new Place {Id=1, Name="1/14", Floor=1, Room="14", Description="Newsroom"},
            new Place {Id=2, Name="0/7", Floor=0, Room="7", Description="Księgowość"},
        };

        private List<DrumModel> _drumModels = new List<DrumModel>
        {
            new DrumModel { Id=0, Name="CX500", Threshold=2 },
            new DrumModel { Id=1, Name="CX600", Threshold=4 },
            new DrumModel { Id=2, Name="CX700", Threshold=3 },
        };

        private List<TonerModel> _tonerModels = new List<TonerModel>
        {
            new TonerModel { Id=0, Name="C500", ColorType=(TonerModel.Color)Enum.Parse(typeof(TonerModel.Color), "Black"), Threshold=2 },
            new TonerModel { Id=1, Name="C600", ColorType=(TonerModel.Color)Enum.Parse(typeof(TonerModel.Color), "Magenta") , Threshold=3 },
            new TonerModel { Id=2, Name="C700", ColorType=(TonerModel.Color)Enum.Parse(typeof(TonerModel.Color), "Cyan"), Threshold=1 },
        };

        private List<PrinterModel> _printerModels = new List<PrinterModel>
        {
            new PrinterModel { Id=0, Name="Lexmark X222", Toners = new List<TonerModel>(), Drums=new List<DrumModel>(), IsColorSupported=false},
            new PrinterModel { Id=1, Name="Lexmark X223", Toners = new List<TonerModel>(), Drums=new List<DrumModel>(), IsColorSupported=false},
            new PrinterModel { Id=2, Name="Lexmark X224", Toners = new List<TonerModel>(), Drums=new List<DrumModel>(), IsColorSupported=false},
        };

        private List<Invoice> _invoices = new List<Invoice>
        {
            new Invoice {Id=0, Date = new DateTime(2020, 3, 2), Price=10000, Number="FK/2020/202"}
        };

        private List<Toner> _toners = new List<Toner>
        {
            new Toner { Id=0, Price=100, SerialNumber="SN8284752" },
            new Toner { Id=1, Price=100, SerialNumber="SN8284753" },
            new Toner { Id=2, Price=100, SerialNumber="SN8284754" },
            new Toner { Id=3, Price=100, SerialNumber="SN8284755" },
            new Toner { Id=4, Price=100, SerialNumber="SN8284756" }
        };

        private List<Drum> _drums = new List<Drum>
        {
            new Drum {Id=0, Price=80, SerialNumber="SN28715982"},
            new Drum {Id=1, Price=80, SerialNumber="SN28715983"},
            new Drum {Id=2, Price=80, SerialNumber="SN28715984"},
            new Drum {Id=3, Price=80, SerialNumber="SN28715985"},
        };

        private List<Printer> _printers = new List<Printer>
        {
            new Printer
            {
                Id=0,
                Name="Drukarka 6a",
                Description="Drukarka, która jest na wykończeniu",
                Ip= System.Net.IPAddress.Parse("10.191.10.10"),                                
                IsOnline = true,
                //Status = new PrinterStatus
                //{
                //    Black = 100,
                //    Cyan = 0,
                //    Magenda = 0,
                //    Yellow = 0
                //},
            },
            new Printer
            {
                Id=1,
                Name="Drukarka sekretariat",
                Description=String.Empty,
                Ip= System.Net.IPAddress.Parse("10.191.10.11"),                                               
                IsOnline = true,
                //Status = new PrinterStatus
                //{
                //    Black = 43,
                //    Cyan = 68,
                //    Magenda = 87,
                //    Yellow = 23
                //},
            },
            new Printer
            {
                Id=2,
                Name="Drukarka newsroom",
                Description="Drukarka na pierwszym piętrze - NewsRoom. Mocno używana.",
                Ip= System.Net.IPAddress.Parse("10.191.10.12"),                                              
                IsOnline = true,
                //Status = new PrinterStatus
                //{
                //    Black = 68,
                //    Cyan = 0,
                //    Magenda = 0,
                //    Yellow = 0
                //},
            },
        };

        private List<Order> _orders = new List<Order>
        {
            new Order { Id=0, Date=DateTime.Now, Drums = new List<Drum>(), Toners = new List<Toner>() },
            new Order { Id=1, Date=DateTime.Now, Drums = new List<Drum>(), Toners = new List<Toner>() },
            new Order { Id=2, Date=DateTime.Now, Drums = new List<Drum>(), Toners = new List<Toner>() },
            new Order { Id=3, Date=DateTime.Now, Drums = new List<Drum>(), Toners = new List<Toner>() }
        };
        public LocalTestDatabaseService()
        {
            foreach(var p in _printerModels)
            {
                p.Toners.Add(_tonerModels[p.Id]);
                p.Drums.Add(_drumModels[p.Id]);                
            }

            foreach(var p in _printers)
            {
                p.Model = _printerModels[p.Id];
                p.Place = _places[p.Id];
            }

            foreach (var t in _toners)
            {
                t.Invoice = _invoices.FirstOrDefault();
                t.Model = _tonerModels[t.Id % 3];
            }

            foreach (var d in _drums)
            {
                d.Invoice = _invoices.FirstOrDefault();
                d.Model = _drumModels[d.Id % 3];
            }

            foreach (var i in _invoices)
            {
                i.Supplier = _suppliers[0];
            }

            foreach(var p in _people)
            {
                p.Department = _departments[p.Id / 2];
            }
            
            foreach (var d in _departments)
            {
                d.Supervisor = _people[d.Id];               
            }

            foreach(var o in _orders)
            {
                o.Client = _people.FirstOrDefault(p => p.Id == o.Id);
                o.Department = _departments[o.Id];
                o.Drums.Add(_drums[o.Id]);
                o.Toners.Add(_toners[o.Id]);
            }
        }

        public event EventHandler<DataChangedEventArgs> DataChanged;
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        public int AddDrumModel(DrumModel drumModel)
        {
            _drumModels.Add(drumModel);
            DataChanged?.Invoke(this, new DataChangedEventArgs(drumModel, DataChangedEventArgs.ChangeType.Add));
            return _drumModels.IndexOf(drumModel);
        }

        public int AddDrum(Drum drum)
        {
            _drums.Add(drum);
            DataChanged?.Invoke(this, new DataChangedEventArgs(drum, DataChangedEventArgs.ChangeType.Add));
            return _drums.IndexOf(drum);
        }        

        public int AddPlace(Place place)
        {
            _places.Add(place);
            DataChanged?.Invoke(this, new DataChangedEventArgs(place, DataChangedEventArgs.ChangeType.Add));
            return _places.IndexOf(place);
        }

        public int AddPrinter(Printer printer)
        {
            _printers.Add(printer);
            DataChanged?.Invoke(this, new DataChangedEventArgs(printer, DataChangedEventArgs.ChangeType.Add));
            return _printers.IndexOf(printer);
        }

        public int AddPrinterModel(PrinterModel printerModel)
        {
            _printerModels.Add(printerModel);
            DataChanged?.Invoke(this, new DataChangedEventArgs(printerModel, DataChangedEventArgs.ChangeType.Add));
            return _printerModels.IndexOf(printerModel);
        }

        public int AddTonerModel(TonerModel tonerModel)
        {
            _tonerModels.Add(tonerModel);
            DataChanged?.Invoke(this, new DataChangedEventArgs(tonerModel, DataChangedEventArgs.ChangeType.Add));
            return _tonerModels.IndexOf(tonerModel);
        }

        public int AddToner(Toner toner)
        {
            _toners.Add(toner);
            DataChanged?.Invoke(this, new DataChangedEventArgs(toner, DataChangedEventArgs.ChangeType.Add));
            return _toners.IndexOf(toner);
        }

        public int AddInvoice(Invoice invoice)
        {
            _invoices.Add(invoice);

            DataChanged?.Invoke(this, new DataChangedEventArgs(invoice, DataChangedEventArgs.ChangeType.Add));
            return _invoices.IndexOf(invoice);
        }

        public int AddSupplier(Supplier supplier)
        {
            _suppliers.Add(supplier);

            DataChanged?.Invoke(this, new DataChangedEventArgs(supplier, DataChangedEventArgs.ChangeType.Add));
            return _suppliers.IndexOf(supplier);
        }

        public int AddDepartment(Department department)
        {
            _departments.Add(department);

            DataChanged?.Invoke(this, new DataChangedEventArgs(department, DataChangedEventArgs.ChangeType.Add));
            return _departments.IndexOf(department);
        }

        public int AddPerson(Person person)
        {
            _people.Add(person);
            DataChanged?.Invoke(this, new DataChangedEventArgs(person, DataChangedEventArgs.ChangeType.Add));
            return _people.IndexOf(person);
        }

        public int AddOrder(Order order)
        {
            _orders.Add(order);
            DataChanged?.Invoke(this, new DataChangedEventArgs(order, DataChangedEventArgs.ChangeType.Add));
            return _orders.IndexOf(order);
        }

        public int EditDrumModel(DrumModel drumModel)
        {
            var item = _drumModels.FirstOrDefault(d => d.Id == drumModel.Id);
            item.Name = drumModel.Name;

            DataChanged?.Invoke(this, new DataChangedEventArgs(drumModel, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }        

        public int EditDrum(Drum drum)
        {
            var item = _drums.FirstOrDefault(d => d.Id == drum.Id);
            item.SerialNumber = drum.SerialNumber;
            item.Invoice = drum.Invoice;
            item.Model = drum.Model;
            item.Price = drum.Price;

            DataChanged?.Invoke(this, new DataChangedEventArgs(drum, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }        

        public int EditPlace(Place place)
        {
            var item = _places.FirstOrDefault(d => d.Id == place.Id);
            item.Name = place.Name;
            item.Description = place.Description;
            item.Floor = place.Floor;
            item.Room = place.Room;

            DataChanged?.Invoke(this, new DataChangedEventArgs(place, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditPrinter(Printer printer)
        {
            var item = _printers.FirstOrDefault(d => d.Id == printer.Id);
            item.Name = printer.Name;
            item.Description = printer.Description;
            item.Ip = printer.Ip;
            item.Model = printer.Model;
            item.Place = printer.Place;

            DataChanged?.Invoke(this, new DataChangedEventArgs(printer, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditPrinterModel(PrinterModel printerModel)
        {
            var item = _printerModels.FirstOrDefault(d => d.Id == printerModel.Id);
            item.Name = printerModel.Name;
            item.Drums = printerModel.Drums;
            item.IsColorSupported = printerModel.IsColorSupported;
            item.Toners = printerModel.Toners;

            DataChanged?.Invoke(this, new DataChangedEventArgs(printerModel, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditTonerModel(TonerModel tonerModel)
        {
            var item = _tonerModels.FirstOrDefault((Func<TonerModel, bool>)(d => d.Id == tonerModel.Id));
            item.Name = tonerModel.Name;
            item.ColorType = tonerModel.ColorType;

            DataChanged?.Invoke(this, new DataChangedEventArgs(tonerModel, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditToner(Toner toner)
        {
            var item = _toners.FirstOrDefault(d => d.Id == toner.Id);
            item.Invoice = toner.Invoice;
            item.Model = toner.Model;
            item.Price = toner.Price;
            item.SerialNumber = toner.SerialNumber;

            DataChanged?.Invoke(this, new DataChangedEventArgs(toner, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditInvoice(Invoice invoice)
        {
            var item = _invoices.FirstOrDefault(d => d.Id == invoice.Id);
            item.Date = invoice.Date;
            item.Number = invoice.Number;
            item.Price = invoice.Price;
            item.Supplier = invoice.Supplier;

            DataChanged?.Invoke(this, new DataChangedEventArgs(invoice, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditSupplier(Supplier supplier)
        {
            var item = _suppliers.FirstOrDefault(d => d.Id == supplier.Id);
            item.Name = supplier.Name;
            item.Phone = supplier.Phone;
            item.Email = supplier.Email;
            item.Description = supplier.Description;

            DataChanged?.Invoke(this, new DataChangedEventArgs(supplier, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditDepartment(Department department)
        {
            var item = _departments.FirstOrDefault(d => d.Id == department.Id);
            item.Name = department.Name;
            item.Id = department.Id;
            item.Supervisor = department.Supervisor;

            DataChanged?.Invoke(this, new DataChangedEventArgs(department, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditOrder(Order order)
        {
            var item = _orders.FirstOrDefault(o => o.Id == order.Id);
            item.Date = order.Date;
            item.Department = order.Department;
            item.Client = order.Client;
            item.Drums = order.Drums;
            item.Toners = order.Toners;                        

            DataChanged?.Invoke(this, new DataChangedEventArgs(order, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public int EditPerson(Person person)
        {
            var item = _people.FirstOrDefault(p => p.Id == person.Id);
            item.Name = person.Name;
            item.Surname = person.Surname;
            item.Department = person.Department;            

            DataChanged?.Invoke(this, new DataChangedEventArgs(person, DataChangedEventArgs.ChangeType.Edit));
            return item.Id;
        }

        public List<DrumModel> GetDrumModels()
        {
            return new List<DrumModel>(_drumModels);
        }

        public List<Drum> GetDrums()
        {
            return new List<Drum>(_drums);
        }

        public List<Place> GetPlaces()
        {
            return new List<Place>(_places);
        }

        public List<PrinterModel> GetPrinterModels()
        {
            return new List<PrinterModel>(_printerModels);
        }

        public List<Printer> GetPrinters()
        {
            return new List<Printer>(_printers);
        }

        public List<TonerModel> GetTonerModels()
        {
            return new List<TonerModel>(_tonerModels);
        }

        public List<Toner> GetToners()
        {
            return new List<Toner>(_toners);
        }

        public List<Invoice> GetInvoices()
        {
            return new List<Invoice>(_invoices);
        }

        public List<Supplier> GetSuppliers()
        {
            return new List<Supplier>(_suppliers);
        }

        public List<Person> GetPeople()
        {
            return new List<Person>(_people);
        }

        public List<Order> GetOrders()
        {
            return new List<Order>(_orders);
        }

        public List<Department> GetDepartments()
        {
            return new List<Department>(_departments);
        }

        public void RaiseDataChanged(object sender, DataChangedEventArgs e)
        {
            DataChanged?.Invoke(sender, e);
        }

        public int RemoveDrum(Drum drum)
        {
            _drums.Remove(drum);

            DataChanged?.Invoke(this, new DataChangedEventArgs(drum, DataChangedEventArgs.ChangeType.Delete));
            return drum.Id;
        }

        public int RemoveDrumModel(DrumModel drumModel)
        {
            _drumModels.Remove(drumModel);

            DataChanged?.Invoke(this, new DataChangedEventArgs(drumModel, DataChangedEventArgs.ChangeType.Delete));
            return drumModel.Id;
        }        

        public int RemovePlace(Place drum)
        {
            _places.Remove(drum);

            DataChanged?.Invoke(this, new DataChangedEventArgs(drum, DataChangedEventArgs.ChangeType.Delete));
            return drum.Id;
        }

        public int RemovePrinter(Printer printer)
        {
            _printers.Remove(printer);

            DataChanged?.Invoke(this, new DataChangedEventArgs(printer, DataChangedEventArgs.ChangeType.Delete));
            return printer.Id;
        }

        public int RemovePrinterModel(PrinterModel printerModel)
        {
            _printerModels.Remove(printerModel);

            DataChanged?.Invoke(this, new DataChangedEventArgs(printerModel, DataChangedEventArgs.ChangeType.Delete));
            return printerModel.Id;
        }

        public int RemoveToner(Toner toner)
        {
            _toners.Remove(toner);

            DataChanged?.Invoke(this, new DataChangedEventArgs(toner, DataChangedEventArgs.ChangeType.Delete));
            return toner.Id;
        }

        public int RemoveTonerModel(TonerModel tonerModel)
        {
            _tonerModels.Remove(tonerModel);

            DataChanged?.Invoke(this, new DataChangedEventArgs(tonerModel, DataChangedEventArgs.ChangeType.Delete));
            return tonerModel.Id;
        }                        

        public int RemoveInvoice(Invoice invoice)
        {
            _invoices.Remove(invoice);

            DataChanged?.Invoke(this, new DataChangedEventArgs(invoice, DataChangedEventArgs.ChangeType.Delete));
            return invoice.Id;
        }                

        public int RemoveSupplier(Supplier supplier)
        {
            _suppliers.Remove(supplier);

            DataChanged?.Invoke(this, new DataChangedEventArgs(supplier, DataChangedEventArgs.ChangeType.Delete));
            return supplier.Id;
        }

        public int RemoveOrder(Order order)
        {
            _orders.Remove(order);

            DataChanged?.Invoke(this, new DataChangedEventArgs(order, DataChangedEventArgs.ChangeType.Delete));
            return order.Id;
        }

        public int RemoveDepartment(Department department)
        {
            _departments.Remove(department);

            DataChanged?.Invoke(this, new DataChangedEventArgs(department, DataChangedEventArgs.ChangeType.Delete));
            return department.Id;
        }

        public int RemovePerson(Person person)
        {
            _people.Remove(person);

            DataChanged?.Invoke(this, new DataChangedEventArgs(person, DataChangedEventArgs.ChangeType.Delete));
            return person.Id;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<DrumModel> GetCompatibleDrums(int printerId)
        {
            throw new NotImplementedException();
        }

        public List<TonerModel> GetCompatibleToners(int printerId)
        {
            throw new NotImplementedException();
        }

        public List<PrinterStatus> GetPrinterStatus()
        {
            throw new NotImplementedException();
        }
    }
}
