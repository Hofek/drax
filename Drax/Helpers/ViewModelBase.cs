﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Drax.Helpers
{

    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {        
        public event PropertyChangedEventHandler PropertyChanged;
     
        protected virtual bool Set<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (object.Equals(storage, value)) return false;
            storage = value;            
            this.OnPropertyChanged(propertyName);
            return true;
        }
        
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void Dispose() { }
    }

}
