﻿using System.Runtime.CompilerServices;

namespace Drax.Helpers
{
    public class ModifyableViewModelBase : ViewModelBase
    {
        private bool _isModified;
        public bool IsModified
        {
            get => _isModified;
            set
            {
                if (_isModified == value)
                    return;

                _isModified = value;
            }
        }
        protected override bool Set<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (!base.Set(ref field, value, propertyName))
                return false;
            IsModified = true;
            return true;
        }
    }
}
