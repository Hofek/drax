﻿using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Order : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private DateTime _date;
        private Person _client;
        private Department _department;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public List<Drum> Drums { get; set; }
        public List<Toner> Toners { get; set; }
        public Person Client 
        {
            get => _client;
            set
            {
                if (value == _client)
                    return;
                _client = value;
                RaisePropertyChanged();
            }
        }
        public Department Department 
        {
            get => _department;
            set
            {
                if (value == _department)
                    return;
                _department = value;
                RaisePropertyChanged();
            }
        }
        public DateTime Date
        {
            get => _date;
            set
            {
                if (value == _date)
                    return;
                _date = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
