﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Department : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private Person _supervisor;

        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;

                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name 
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;

                _name = value;
                RaisePropertyChanged();
            }
        }
        public Person Supervisor 
        { 
            get => _supervisor; 
            set
            {
                if (value == _supervisor)
                    return;

                _supervisor = value;
                RaisePropertyChanged();
            }
        }
        

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
