﻿using System;
using static DraxCommon.Model.Enums;

namespace DraxCommon.Model
{
    [Serializable]
    public class TcpMessage
    {
        public MessageType MessageType { get; }
        //public Type Type { get; }
        public string MethodName { get; }
        public object Value { get; }

        /// <summary>
        /// Used to send message TO SERVER.
        /// </summary>
        /// <param name="methodName">MethodName from IDatabaseService interface</param>
        /// <param name="value">Optional parameter (if edit, remove etc.)</param>
        public TcpMessage(MessageType messageType, string methodName, object value = null)
        {
            MessageType = messageType;
            MethodName = methodName;
            Value = value;
        }

        /// <summary>
        /// Used to send message TO CLIENT
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="value"></param>
        public TcpMessage(MessageType messageType, object value)
        {
            MessageType = messageType;
            Value = value;
        }
    }
}
