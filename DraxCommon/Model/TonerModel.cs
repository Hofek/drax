﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class TonerModel : IMaterial, INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private int _threshold;
        private int _count;

        public enum Color
        {
            Black,
            Cyan,
            Magenta,
            Yellow            
        }      
        
        public Color ColorType { get; set; }
        
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;
                _name = value;
                RaisePropertyChanged();
            }
        }
        public int Threshold
        {
            get => _threshold;
            set
            {
                if (value == _threshold)
                    return;
                _threshold = value;
                RaisePropertyChanged();
            }
        }
        public int Count
        {
            get => _count;
            set
            {
                if (value == _count)
                    return;
                _count = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
