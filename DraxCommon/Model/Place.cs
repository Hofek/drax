﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Place : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private int _floor;
        private string _room;
        private string _description;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;
                _name = value;
                RaisePropertyChanged();
            }
        }
        public int Floor 
        {
            get => _floor; 
            set
            {
                if (value == _floor)
                    return;
                _floor = value;
                RaisePropertyChanged();
            }
        }
        public string Room 
        {
            get => _room; 
            set
            {
                if (value == _room)
                    return;
                _room = value;
                RaisePropertyChanged();
            }
        }
        public string Description 
        {
            get => _description;
            set
            {
                if (value == _description)
                    return;
                _description = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
