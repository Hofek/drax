﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DraxCommon.Model.Args
{
    public class DataChangedEventArgs : EventArgs
    {
        public enum ChangeType
        {
            Add,
            Edit,
            Delete
        };
        public object Value { get; }
        public ChangeType Type { get; }
        public DataChangedEventArgs(object value, ChangeType type)
        {
            Value = value;
            Type = type;
        }
    }
}
