﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DraxCommon.Model.Args
{
    public class SingleValueEventArgs<T> : EventArgs
    {
        public T Item { get; }
        public SingleValueEventArgs(T item)
        {
            Item = item;
        }
    }
}
