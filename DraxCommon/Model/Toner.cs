﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Toner : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _serialNumber;
        private double _price;
        private TonerModel _model;
        private Order _order;
        private Invoice _invoice;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string SerialNumber 
        {
            get => _serialNumber;
            set
            {
                if (value == _serialNumber)
                    return;
                _serialNumber = value;
                RaisePropertyChanged();
            }
        }
        public double Price 
        {
            get => _price;
            set
            {
                if (value == _price)
                    return;
                _price = value;
                RaisePropertyChanged();
            }
        }

        public TonerModel Model 
        {
            get => _model;
            set
            {
                if (_model == value)
                    return;
                _model = value;
                RaisePropertyChanged();
            }
        }
        public Invoice Invoice 
        {
            get => _invoice;
            set
            {
                if (value == _invoice)
                    return;
                _invoice = value;
                RaisePropertyChanged();
            }
        }
        public Order Order
        {
            get => _order;
            set
            {
                if (value == _order)
                    return;
                _order = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
