﻿using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DraxCommon.Model
{
    [Serializable]
    public class Person : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private string _surname;
        private Department _department;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;
                _name = value;
                RaisePropertyChanged();
            }
        }
        public string Surname 
        {
            get => _surname;
            set
            {
                if (value == _surname)
                    return;
                _surname = value;
                RaisePropertyChanged();
            }
        }
        public Department Department 
        {
            get => _department;
            set
            {
                if (value == _department)
                    return;
                _department = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
