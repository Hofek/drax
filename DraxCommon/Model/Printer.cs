﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Printer : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private string _description;
        private PrinterModel _model;
        private IPAddress _ip;
        private bool _isOnline;        
        private Place _place;
        private PrinterStatus _status;

        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;
                _name = value;
                RaisePropertyChanged();
            }
        }
        public PrinterModel Model 
        {
            get => _model;
            set
            {
                if (value == _model)
                    return;
                _model = value;
                RaisePropertyChanged();
            }
        }
        public IPAddress Ip 
        {
            get => _ip;
            set
            {
                if (value == _ip)
                    return;
                _ip = value;
                RaisePropertyChanged();
            }
        }
        public string Description
        {
            get => _description;
            set
            {
                if (value == _description)
                    return;
                _description = value;
                RaisePropertyChanged();
            }
        }
        public bool IsOnline 
        {
            get => _isOnline;
            set
            {
                if (value == _isOnline)
                    return;
                _isOnline = value;
                RaisePropertyChanged();
            }
        }        
        
        public Place Place 
        {
            get => _place;
            set
            {
                if (value == _place)
                    return;
                _place = value;
                RaisePropertyChanged();
            }
        }

        public PrinterStatus Status
        {
            get => _status;
            set
            {
                if (value == _status)
                    return;
                _status = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
