﻿using DraxCommon.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class PrinterModel : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private bool _isColorSupported;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;
                _name = value;
                RaisePropertyChanged();
            }
        }
        public List<TonerModel> Toners { get; set; }
        public List<DrumModel> Drums { get; set; }
        public bool IsColorSupported 
        {
            get => _isColorSupported; 
            set
            {
                if (value == _isColorSupported)
                    return;
                _isColorSupported = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
