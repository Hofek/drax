﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Invoice : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _number;
        private double _price;
        private DateTime _date;
        private Supplier _supplier;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Number 
        {
            get => _number; 
            set
            {
                if (value == _number)
                    return;
                _number = value;
                RaisePropertyChanged();
            }
        }
        public double Price
        {
            get => _price;
            set
            {
                if (value == _price)
                    return;
                _price = value;
                RaisePropertyChanged();
            }
        }
        public DateTime Date 
        {
            get => _date; 
            set
            {
                if (value == _date)
                    return;
                _date = value;
                RaisePropertyChanged();
            }
        }
        public Supplier Supplier 
        {
            get => _supplier;
            set
            {
                if (value == _supplier)
                    return;
                _supplier = value;
                RaisePropertyChanged();
            }
        }
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
