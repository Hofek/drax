﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class Supplier : INotifyPropertyChanged, DbClass
    {
        private int _id;
        private string _name;
        private string _phone;
        private string _email;
        private string _description;
        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                    return;
                _name = value;
                RaisePropertyChanged();
            }
        }
        public string Phone 
        {
            get => _phone;
            set
            {
                if (value == _phone)
                    return;
                _phone = value;
                RaisePropertyChanged();
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                if (value == _email)
                    return;
                _email = value;
                RaisePropertyChanged();
            }
        }
        public string Description
        {
            get => _description;
            set
            {
                if (value == _description)
                    return;
                _description = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
