﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DraxCommon.Model.Interfaces
{
    public interface IWindowManager
    {
        void Close(object content);
        bool? CreateDialog(object content);
        bool? CreateMessageBox(string content);        
    }
}
