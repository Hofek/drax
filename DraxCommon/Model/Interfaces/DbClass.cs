﻿namespace DraxCommon.Model.Interfaces
{
    public interface DbClass
    {
        int Id { get; set; }
    }
}
