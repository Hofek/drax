﻿
namespace DraxCommon.Model.Interfaces
{
    public interface IMaterial
    {
        int Id { get; }
        string Name { get; } 
        int Threshold { get; }
        int Count { get; }
    }
}
