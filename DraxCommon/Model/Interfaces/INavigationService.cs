﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DraxCommon.Model.Interfaces
{
    public interface INavigationService
    {        
        void RaiseMainViewChange(string obj);
    }
}
