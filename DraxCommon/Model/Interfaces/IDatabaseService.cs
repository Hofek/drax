﻿using DraxCommon.Model.Args;
using System;
using System.Collections.Generic;

namespace DraxCommon.Model.Interfaces
{
    public interface IDatabaseService : IDisposable
    {
        int AddPrinter(Printer printer);
        int AddToner(Toner toner);
        int AddDrum(Drum drun);
        int AddPrinterModel(PrinterModel printer);
        int AddTonerModel(TonerModel toner);
        int AddDrumModel(DrumModel drun);
        int AddPlace(Place drun);
        int AddInvoice(Invoice invoice);
        int AddSupplier(Supplier supplier);
        int AddDepartment(Department department);
        int AddPerson(Person person);
        int AddOrder(Order order);
        int EditPrinter(Printer printer);
        int EditToner(Toner toner);
        int EditDrum(Drum drun);
        int EditPrinterModel(PrinterModel printer);
        int EditTonerModel(TonerModel toner);
        int EditDrumModel(DrumModel drun);
        int EditPlace(Place drun);
        int EditInvoice(Invoice invoice);
        int EditSupplier(Supplier supplier);
        int EditDepartment(Department department);
        int EditOrder(Order order);
        int EditPerson(Person person);
        int RemovePrinter(Printer printer);
        int RemoveToner(Toner toner);
        int RemoveDrum(Drum drum);
        int RemovePrinterModel(PrinterModel printer);
        int RemoveTonerModel(TonerModel toner);
        int RemoveDrumModel(DrumModel drum);
        int RemovePlace(Place drun);
        int RemoveInvoice(Invoice invoice);
        int RemoveSupplier(Supplier supplier);
        int RemoveDepartment(Department department);
        int RemovePerson(Person person);
        int RemoveOrder(Order order);
        List<Printer> GetPrinters();
        List<Toner> GetToners();
        List<Drum> GetDrums();
        List<PrinterModel> GetPrinterModels();
        List<TonerModel> GetTonerModels();
        List<DrumModel> GetDrumModels();
        List<Place> GetPlaces();
        List<Invoice> GetInvoices();
        List<Supplier> GetSuppliers();
        List<Department> GetDepartments();
        List<Order> GetOrders();
        List<Person> GetPeople();
        List<PrinterStatus> GetPrinterStatus();
        List<DrumModel> GetCompatibleDrums(int printerId);
        List<TonerModel> GetCompatibleToners(int printerId);
        event EventHandler<DataChangedEventArgs> DataChanged;
        void RaiseDataChanged(object sender, DataChangedEventArgs e);
        event EventHandler Connected;
        event EventHandler Disconnected;
    }
}
