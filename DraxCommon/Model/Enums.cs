﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DraxCommon.Model
{
    public static class Enums
    {
        public enum MessageType
        {
            Response,
            Query,
            Add,
            Edit,
            Remove
        };
    }
}
