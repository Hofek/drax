﻿using DraxCommon.Model.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraxCommon.Model
{
    [Serializable]
    public class PrinterStatus : INotifyPropertyChanged, DbClass
    {
        private int _black;
        private int _magenda;
        private int _cyan;
        private int _yellow;
        private int _id;
        private Printer _printer;

        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;
                _id = value;
                RaisePropertyChanged();
            }
        }
        public int Black 
        {
            get => _black;
            set
            {
                if (value == _black)
                    return;
                _black = value;
                RaisePropertyChanged();
            }
        }
        public int Magenda 
        {
            get => _magenda;
            set
            {
                if (value == _magenda)
                    return;
                _magenda = value;
                RaisePropertyChanged();
            }
        }
        public int Cyan 
        {
            get => _cyan;
            set
            {
                if (value == _cyan)
                    return;
                _cyan = value;
                RaisePropertyChanged();
            }
        }
        public int Yellow 
        {
            get => _yellow;
            set
            {
                if (value == _yellow)
                    return;
                _yellow = value;
                RaisePropertyChanged();
            }
        }

        public Printer Printer 
        { 
            get => _printer;
            set
            {
                if (value == _printer)
                    return;
                _printer = value;
                RaisePropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
