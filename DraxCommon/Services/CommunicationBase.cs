﻿using DraxCommon.Model;
using DraxCommon.Model.Args;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace DraxCommon.Services
{
    public abstract class CommunicationBase : IDisposable
    {
        public TcpClient Client { get; protected set; }
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        
        private readonly ConcurrentQueue<TcpMessage> _sendQueue = new ConcurrentQueue<TcpMessage>();
        public event EventHandler Disconnected;
        private readonly SemaphoreSlim _sendSemaphore = new SemaphoreSlim(1);
        public event EventHandler<SingleValueEventArgs<TcpMessage>> MessageReceived;
        protected void Send(TcpMessage message)
        {
            _sendQueue.Enqueue(message);
            
            if (_sendSemaphore.CurrentCount == 0)
                _sendSemaphore.Release();
        }

        protected void StartCommunicationThreads()
        {
            var sendThread = new Thread(new ThreadStart(Sender));
            sendThread.Name = $"Sender Thread {Client.Client.LocalEndPoint}";
            sendThread.IsBackground = true;
            
            var listenThread = new Thread(new ThreadStart(Listener));
            listenThread.Name = $"Listener Thread {Client.Client.LocalEndPoint}";
            listenThread.IsBackground = true;

            listenThread.Start();
            sendThread.Start();
        }

        private void Sender()
        {
            var stream = Client.GetStream();            
            var bf = new BinaryFormatter();
            while(!_cancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    if (_sendQueue.IsEmpty)
                        _sendSemaphore.Wait(_cancellationTokenSource.Token);
                    
                    if (!_sendQueue.TryDequeue(out var message))
                        continue;

                    using (var memoryStream = new MemoryStream())
                    {
                        //WriteData
                        memoryStream.Position = 8;
                        bf.Serialize(memoryStream, message);
                        //WriteSize
                        memoryStream.Position = 0;
                        memoryStream.Write(BitConverter.GetBytes(memoryStream.Length-8), 0, 8);
                        memoryStream.Position = 0;
                        //Send
                        memoryStream.CopyTo(stream);                        
                    }
                }
                catch (Exception ex)
                {
                    if (ex is OperationCanceledException)
                        Debug.WriteLine("Sender stopped");
                    else
                        Debug.WriteLine($"Sender Error {ex}");
                }                
            }
        }

        private void Listener()
        {
            var stream = Client.GetStream();
            var bf = new BinaryFormatter();

            int bytesRead = 0;
            
            byte[] dataBuffer = null;
            int dataBufferSize = 0;
            
            while(!_cancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    if (bytesRead == 0)
                    {
                        var sizeBuffer = new byte[8];
                        if ((bytesRead = stream.Read(sizeBuffer, 0, 8)) == 8)
                        {
                            //Deserialize size of message
                            dataBufferSize = (int)BitConverter.ToInt64(sizeBuffer, 0);

                            //Receive message
                            dataBuffer = new byte[dataBufferSize];
                            bytesRead = stream.Read(dataBuffer, 0, dataBufferSize);                            
                        }
                    }
                    else
                    {
                        bytesRead += stream.Read(dataBuffer, bytesRead, dataBuffer.Length-bytesRead);                        
                    }

                    if (bytesRead == dataBufferSize)
                    {
                        bytesRead = 0;
                        MessageReceived?.Invoke(this, new SingleValueEventArgs<TcpMessage>(((TcpMessage)bf.Deserialize(new MemoryStream(dataBuffer)))));
                    }
                    else
                        continue;
                }
                catch(Exception ex)
                {
                    if (ex is IOException)                    
                        Dispose();                      
                    
                    Debug.WriteLine($"Error in listener! {ex.Message}");                   
                }
            }
        }

        public void Dispose()
        {
            Debug.WriteLine("Disposing Client");
            Disconnected?.Invoke(this, EventArgs.Empty);
            _cancellationTokenSource.Cancel();
            Client?.Client.Close();
            Client?.Close();            
        }
    }
}
