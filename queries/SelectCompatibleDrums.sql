SELECT drummodel.* 
FROM ((printerdrum
INNER JOIN printermodel ON printerdrum.printermodel_idPrinterModel = printermodel.idPrinterModel)
INNER JOIN drummodel ON printerdrum.drummodel_idDrumModel = drummodel.idDrumModel)
WHERE printermodel.idPrinterModel = @idPrinterModel;