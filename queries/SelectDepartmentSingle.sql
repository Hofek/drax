SELECT department.* , personinfo.PersonInfoName, personinfo.PersonInfoSurname
FROM department
INNER JOIN personinfo
ON department.personinfo_idPersonInfo=personinfo.idPersonInfo
WHERE department.idDepartment=@department.idDepartment;