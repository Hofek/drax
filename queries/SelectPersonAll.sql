SELECT personinfo.idPersonInfo, personinfo.PersonInfoName, personinfo.PersonInfoSurname, department.DepartmentName
FROM personinfo
INNER JOIN department ON personinfo.department_idDepartment = department.idDepartment;