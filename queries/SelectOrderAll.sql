SELECT orderinfo.idOrderInfo, orderinfo.OrderInfoDate, personinfo.PersonInfoName, personinfo.PersonInfoSurname, department.DepartmentName
FROM (( orderinfo
INNER JOIN personinfo ON orderinfo.personinfo_idPersonInfo = personinfo.idPersonInfo)
INNER JOIN department ON orderinfo.department_idDepartment = department.idDepartment);