SELECT printerinfo.idPrinterInfo, printerinfo.PrinterInfoName, printerinfo.PrinterInfoIP, printerinfo.PrinterInfoDescription, place.PlaceName, place.PlaceFloor, place.PlaceRoom, printermodel.PrinterModelName
FROM ((printerinfo
INNER JOIN place ON printerinfo.place_idPlace = place.idPlace)
INNER JOIN printermodel ON printerinfo.printermodel_idPrinterModel = printermodel.idPrinterModel);