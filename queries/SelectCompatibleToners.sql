SELECT tonermodel.* 
FROM ((printertoner
INNER JOIN printermodel ON printertoner.printermodel_idPrinterModel = printermodel.idPrinterModel)
INNER JOIN tonermodel ON printertoner.tonermodel_idTonerModel = tonermodel.idTonerModel)
WHERE printermodel.idPrinterModel = @idPrinterModel