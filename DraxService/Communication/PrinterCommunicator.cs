﻿using DraxCommon.Model;
using DraxCommon.Model.Interfaces;
using DraxService.Services;
using SnmpSharpNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DraxService.Communication
{
    public class PrinterCommunicator : IDisposable
    {
        private List<UdpTarget> _targets = new List<UdpTarget>();
        private readonly IDatabaseService _databaseService;
        private System.Timers.Timer _timer;

        public PrinterCommunicator(IDatabaseService dbService)
        {
            _databaseService = dbService;
            ((DatabaseService)_databaseService).DataChanged += PrinterCommunicator_DataChanged;
            Init();
            RequestAll();

            _timer = new System.Timers.Timer(300000);
            _timer.Elapsed += Timer_Elapsed;
            _timer.Enabled = true;
            _timer.Start();            
        }

        private void Init()
        {
            _targets = new List<UdpTarget>();
            foreach (var printer in _databaseService.GetPrinters())
                _targets.Add(new UdpTarget(printer.Ip));
        }

        private void RequestAll()
        {
            Task.Run(() => 
            {
                foreach (var target in _targets.ToList())
                    RequestStatus(target);
            });
        }

        private void PrinterCommunicator_DataChanged(object sender, DraxCommon.Model.Args.DataChangedEventArgs e)
        {
            if (e.Value is Printer printer)
                Init();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            RequestAll();
            _timer.Start();
        }

        public void RequestStatus(UdpTarget target)
        {
            try
            {
                Debug.WriteLine(target.Address);
                SnmpV1Packet packet = new SnmpV1Packet();
                // Set the community name
                packet.Community.Set("public");
                // Set the Pdu type
                packet.Pdu.Type = PduType.Get;
                // Add an Oid for the SNMP-Get operation
                packet.Pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1");
                packet.Pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");


                AgentParameters aparam = new AgentParameters(SnmpVersion.Ver1, new OctetString("public"));
                var response = target.Request(packet.Pdu, aparam) as SnmpV1Packet;

                if (response == null)
                    return;

                // Check if we received an SNMP error from the agent
                if (response.Pdu.ErrorStatus != 0)
                    return;

                if (Int32.TryParse(response.Pdu[0].Value.ToString(), out var firstValue))
                {
                    if (Int32.TryParse(response.Pdu[1].Value.ToString(), out var secondValue))
                    {
                        var tonerLevel = (firstValue * 100) / secondValue;
                        ((DatabaseService)_databaseService).EditPrinterStatus(new PrinterStatus { Black = tonerLevel, Printer = new Printer { Ip = target.Address } });
                    }
                }
            }
            catch
            {
                Debug.WriteLine($"Error executing SNMP {target.Address}");
            }
        }

        public void Dispose()
        {
            _timer.Stop();
        }
    }
}
