﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using DraxService.Model;

namespace DraxService.Communication
{
    public class ClientManager : IDisposable
    {
        private readonly IDatabaseService _dbService;
        private readonly TcpListener _serverListener;
        private readonly List<Server> _servers = new List<Server>();
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly ConcurrentQueue<ClientQuery> _clientQueries = new ConcurrentQueue<ClientQuery>();
        private readonly SemaphoreSlim _queriesSemaphore = new SemaphoreSlim(1);
        
        public ClientManager(IDatabaseService dbService, int port)
        {
            _dbService = dbService;
            _dbService.DataChanged += DbService_DataChanged;
            _ = QueryHandler();
            _serverListener = new TcpListener(IPAddress.Any, port);
            _ = Listen();
        }

        private void DbService_DataChanged(object sender, DataChangedEventArgs e)
        {
            if (e.Value is PrinterStatus status)            
                NotifyClients(new TcpMessage(Enums.MessageType.Edit, status));            
        }

        private async Task Listen()
        {
            _serverListener.Start();
            while(!(_cancellationTokenSource.IsCancellationRequested))
            {
                var client = await _serverListener.AcceptTcpClientAsync();
                var server = new Server(client);
                server.MessageReceived += Server_MessageReceived;
                server.Disconnected += Server_Disconnected;
                _servers.Add(server);
                server.Start();
            }
            _serverListener.Stop();
        }

        private void Server_Disconnected(object sender, EventArgs e)
        {
            _servers.Remove(sender as Server);
        }

        private void Server_MessageReceived(object sender, SingleValueEventArgs<TcpMessage> e)
        {
            if (!(sender is Server server))
                return;
            Debug.WriteLine($"Received message of type {e.Item.MessageType}");
            _clientQueries.Enqueue(new ClientQuery((IPEndPoint)server.Client.Client.RemoteEndPoint, e.Item));
            _queriesSemaphore.Release();
        }

        private async Task QueryHandler()
        {
            while(!_cancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    if (_clientQueries.IsEmpty)
                        await _queriesSemaphore.WaitAsync(_cancellationTokenSource.Token);

                    if (!_clientQueries.TryDequeue(out var query))
                        continue;

                    Debug.WriteLine($"Executing query");
                    var result = ExecuteQuery(query.Message);

                    SendResponse(query.Client, result);

                    if (result is int && (int)result != -1)
                    {
                        ((DbClass)query.Message.Value).Id = (int)result;                        
                        NotifyClients(query.Message);
                    }                        
                }

                catch(Exception ex)
                {
                    Debug.WriteLine($"Error in QueryHandler task. {ex.Message}");
                }
            }
        }

        private void SendResponse(IPEndPoint endPoint, object response)
        {
            Debug.WriteLine($"Sending response to {endPoint.Address}");
            _servers.FirstOrDefault(s => s.Client.Client.RemoteEndPoint == endPoint).SendResponse(response);
        }
        private object ExecuteQuery(TcpMessage connectionMessage)
        {
            var method = _dbService.GetType().GetMethod(connectionMessage.MethodName);
            if (method.GetParameters().Length > 0)
                return method.Invoke(_dbService, new object[] { connectionMessage.Value });
            else
                return method.Invoke(_dbService, null);
        }

        private void NotifyClients(TcpMessage tcpMessage)
        {
            foreach (var server in _servers)
                server.SendResponse(tcpMessage.Value, tcpMessage.MessageType);
        }

        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _serverListener.Stop();
            foreach (var server in _servers.ToList())
                server.Dispose();
        }
    }
}
