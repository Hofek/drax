﻿using DraxCommon.Model;
using DraxCommon.Services;
using System;
using System.Net.Sockets;


namespace DraxService.Communication
{
    public class Server : CommunicationBase
    {
        public Server(TcpClient client)
        {
            Client = client;
        }
        public void Start()
        {
            StartCommunicationThreads();
            Console.WriteLine($"[{Client.Client.RemoteEndPoint}] Connection established!");
        }

        public void SendResponse(object response, Enums.MessageType messageType = Enums.MessageType.Response)
        {
            var message = new TcpMessage(messageType, response);
            Send(message);
        }
    }
}
