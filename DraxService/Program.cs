﻿using DraxCommon.Helpers;
using DraxCommon.Model.Interfaces;
using DraxService.Communication;
using DraxService.Model;
using DraxService.Services;
using System;
using System.IO;
using System.ServiceProcess;
using System.Xml.Serialization;

namespace DraxService
{
    class Program
    {
        private static Config _config = null;
        private static IDatabaseService _dbService = null;
        private static PrinterCommunicator _printerCommunicator = null;
        private static ClientManager _clientManager = null;

        #region Nested classes to support running as service
        private const string ServiceName = "Drax";

        public class Service : ServiceBase
        {
            public Service()
            {
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
#if DEBUG
                System.Diagnostics.Debugger.Launch();
#endif
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                Program.Start(args);

            }

            protected override void OnStop()
            {
                Program.Stop();
            }
        }
        #endregion

        private static void Start(string[] args)
        {
            if (!File.Exists("configuration.xml"))
                return;

            using (var reader = new StreamReader("configuration.xml"))
            {
                var serializer = new XmlSerializer(typeof(Config));
                _config = (Config)serializer.Deserialize(reader);                
            }
            _dbService = new DatabaseService(_config.SqlServer);
            _printerCommunicator = new PrinterCommunicator(_dbService);
            _clientManager = new ClientManager(_dbService, _config.Port);
        }

        private static void Stop()
        {
            _clientManager?.Dispose();
            _dbService?.Dispose();
        }

        static void Main(string[] args)
        {
            bool exit = false;
            if (!Environment.UserInteractive)
                using (var service = new Service())
                    ServiceBase.Run(service);
            else
            {
                Start(args);

                if (_config == null)
                    Console.WriteLine("Configuration read failed.");

                while (!exit)
                {
                    Console.Write("> ");
                    string command = Console.ReadLine();

                    switch (command.ToLower())
                    {
                        case "quit":
                        case "q":
                            {
                                exit = true;
                                break;
                            }
                        case "install":
                            {
                                var serviceInstaller = new Helpers.ServiceInstaller();

                                if (serviceInstaller.Install())
                                    Console.WriteLine("Service installed successfully");
                                else
                                    Console.WriteLine("Error. Try running again with admin rights. Check if service already exists.");

                                break;
                            }

                        case "uninstall":
                            {
                                var serviceInstaller = new Helpers.ServiceInstaller();

                                if (serviceInstaller.Uninstall())
                                    Console.WriteLine("Service uninstalled successfully");
                                else
                                    Console.WriteLine("Error. Try running again with admin rights. Check if service already exists.");

                                break;
                            }

                        case "configure":
                            {
                                var config = new Config() { SqlServer = new SqlServer() };
                                
                                Console.Write("Enter Db Server name: ");
                                config.SqlServer.Server = Console.ReadLine();
                                Console.Write("Enter Db listening port: ");
                                if (!Int32.TryParse(Console.ReadLine(), out var dbPort))
                                {
                                    Console.WriteLine("Incorrect value!");
                                    break;
                                }
                                config.SqlServer.Port = dbPort;
                                Console.Write("Enter Database name: ");
                                config.SqlServer.Database = Console.ReadLine();
                                Console.Write("Enter Db username: ");
                                config.SqlServer.Username = Console.ReadLine();
                                Console.Write("Enter Db password: ");
                                config.SqlServer.Password = Encryption.Encrypt(Console.ReadLine());
                                Console.Write("Enter service listening port: ");
                                if (!Int32.TryParse(Console.ReadLine(), out var port))
                                {
                                    Console.WriteLine("Incorrect value!");
                                    break;
                                }
                                config.Port = port;

                                using (var writer = new StreamWriter("configuration.xml"))
                                {
                                    var serializer = new XmlSerializer(typeof(Config));
                                    serializer.Serialize(writer, config);
                                }

                                Console.WriteLine("Settings saved! Restart service to apply changes.");
                                break;
                            }

                        case "help":
                            {
                                Console.WriteLine("quit, q - exit");
                                Console.WriteLine("install - install service");
                                Console.WriteLine("uninstall - uninstall service");
                                Console.WriteLine("configure - configure service");
                                break;
                            }

                        default:
                            {
                                Console.WriteLine("Command not found!");
                                break;
                            }
                    }                    
                    Console.WriteLine();
                }
                Stop();
            }
        }
    }
}
