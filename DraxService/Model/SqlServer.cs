﻿using DraxCommon.Helpers;
using System.Xml.Serialization;

namespace DraxService.Model
{
    public class SqlServer
    {
        [XmlAttribute]
        public string Server { get; set; }
        [XmlAttribute]
        public string Database { get; set; }
        [XmlAttribute]
        public string Username { get; set; }
        [XmlAttribute]
        public string Password { get; set; }
        [XmlAttribute]
        public int Port{ get; set; }
        public string GetConnectionString()
        {
            return $"Server={Server}; Port={Port}; Database={Database}; Uid={Username}; Pwd={Encryption.Decrypt(Password)};";
        }
    }
}
