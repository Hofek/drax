﻿using System.Xml.Serialization;

namespace DraxService.Model
{
    public class Config
    {
        [XmlElement]
        public SqlServer SqlServer { get; set; }
        [XmlElement]
        public int Port { get; set; }
    }
}
