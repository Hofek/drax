﻿using DraxCommon.Model;
using System.Net;

namespace DraxService.Model
{
    public class ClientQuery
    {
        public IPEndPoint Client { get; }
        public TcpMessage Message { get; }

        public ClientQuery(IPEndPoint client, TcpMessage message)
        {
            Client = client;
            Message = message;
        }
    }
}
