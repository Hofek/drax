﻿using DraxCommon.Model;
using DraxCommon.Model.Args;
using DraxCommon.Model.Interfaces;
using DraxService.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;

namespace DraxService.Services
{
    public class DatabaseService : IDatabaseService
    {
        private readonly SqlServer _sqlServerMeta;
        private readonly MySqlConnection _sqlConnection;
        public event EventHandler<DataChangedEventArgs> DataChanged;
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        public DatabaseService(SqlServer sqlServerConfig)
        {
            _sqlServerMeta = sqlServerConfig;            
            _sqlConnection = new MySqlConnection(_sqlServerMeta.GetConnectionString());
            _sqlConnection.Open();
        }

        public int AddPrinterStatus(PrinterStatus printerStatus)
        {
            int id = -1;
            var query = @"INSERT INTO printerstatus(PrinterStatusTonerBlack, printerinfo_idPrinterInfo)
                          VALUES(@PrinterStatusTonerBlack, @printermodel_idPrinterModel); ";

            var transcation = _sqlConnection.BeginTransaction();
            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PrinterStatusTonerBlack", printerStatus.Black);
                    cmd.Parameters.AddWithValue("@printermodel_idPrinterModel", printerStatus.Printer.Id);
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }


                transcation.Commit();
                return id;
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int AddDepartment(Department department)
        {
            int id = -1;
            var query = "INSERT INTO department (DepartmentName, personinfo_idPersonInfo) VALUES (@DepartmentName, @personinfo_idPersonInfo);";

            var transcation = _sqlConnection.BeginTransaction();
            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DepartmentName", department.Name);
                    cmd.Parameters.AddWithValue("@personinfo_idPersonInfo", department.Supervisor.Id);
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }

        }

        public int AddDrum(Drum drum)
        {
            int id = -1;
            var query = @"INSERT INTO drum (DrumSerialNumber, DrumPrice, document_idDocument, drummodel_idDrumModel) 
                              VALUES (@DrumSerialNumber, @DrumPrice, @document_idDocument, @drummodel_idDrumModel);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DrumSerialNumber", drum.SerialNumber);
                    cmd.Parameters.AddWithValue("@DrumPrice", drum.Price);
                    cmd.Parameters.AddWithValue("@document_idDocument", drum.Invoice.Id);
                    cmd.Parameters.AddWithValue("@drummodel_idDrumModel", drum.Model.Id);
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddDrumModel(DrumModel drumModel)
        {
            int id = -1;
            var query = "INSERT INTO drummodel (DrumModelName, DrumModelThreshold) VALUES (@DrumModelName, @DrumModelThreshold);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DrumModelName", drumModel.Name);
                    cmd.Parameters.AddWithValue("@DrumModelThreshold", drumModel.Threshold);
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddInvoice(Invoice invoice)
        {
            int id = -1;
            var query = @"INSERT INTO document (DocumentNumber, DocumentPrice, supplier_idsupplier, DocumentDate) 
                              VALUES (@DocumentNumber, @DocumentPrice, @DocumentSupplier, @DocumentDate);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DocumentNumber", invoice.Number);
                    cmd.Parameters.AddWithValue("@DocumentPrice", invoice.Price);
                    cmd.Parameters.AddWithValue("@DocumentSupplier", invoice.Supplier.Id);
                    cmd.Parameters.AddWithValue("@DocumentDate", invoice.Date);
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddOrder(Order order)
        {
            int orderId = -1;
            var queryOrder = @"INSERT INTO orderinfo(OrderInfoDate, personinfo_idPersonInfo, department_idDepartment) 
                              VALUES (@OrderInfoDate, @personinfo_idPersonInfo, @department_idDepartment);";

            var updateTonerQuery = @"UPDATE toner SET orderinfo_idOrderInfo=@orderId WHERE idToner=@tonerId;";
            var updateDrumQuery = @"UPDATE drum SET orderinfo_idOrderInfo=@orderId WHERE idDrum=@drumId;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(queryOrder, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@OrderInfoDate", order.Date);
                    cmd.Parameters.AddWithValue("@personinfo_idPersonInfo", order.Client.Id);
                    cmd.Parameters.AddWithValue("@department_idDepartment", order.Department.Id);
                    cmd.ExecuteNonQuery();
                    orderId = (int)cmd.LastInsertedId;
                }

                foreach (var toner in order.Toners)
                {
                    using (var cmd = new MySqlCommand(updateTonerQuery, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@tonerId", toner.Id);                     
                        cmd.Parameters.AddWithValue("@orderId", orderId);
                        cmd.ExecuteNonQuery();
                    }
                }

                foreach (var drum in order.Drums)
                {
                    using (var cmd = new MySqlCommand(updateDrumQuery, _sqlConnection))
                    {
                        cmd.Transaction = transcation;                        
                        cmd.Parameters.AddWithValue("@drumId", drum.Id);
                        cmd.Parameters.AddWithValue("@orderId", orderId);
                        cmd.ExecuteNonQuery();
                    }
                }

            
                transcation.Commit();
                return orderId;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }            
        }

        public int AddPerson(Person person)
        {
            int id = -1;
            var query = @"INSERT INTO personinfo(PersonInfoName, PersonInfoSurname, department_idDepartment) 
                          VALUES (@PersonInfoName, @PersonInfoSurname, @department_idDepartment);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PersonInfoName", person.Name);
                    cmd.Parameters.AddWithValue("@PersonInfoSurname", person.Surname);
                    if (person.Department == null)
                        cmd.Parameters.AddWithValue("@department_idDepartment", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@department_idDepartment", person.Department.Id);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddPlace(Place place)
        {
            int id = -1;
            var query = @"INSERT INTO place (PlaceName, PlaceFloor, PlaceRoom, PlaceDescription) 
                          VALUES (@PlaceName, @PlaceFloor, @PlaceRoom, @PlaceDescription);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PlaceName", place.Name);
                    cmd.Parameters.AddWithValue("@PlaceFloor", place.Floor);                
                    cmd.Parameters.AddWithValue("@PlaceRoom", place.Room);                
                    cmd.Parameters.AddWithValue("@PlaceDescription", place.Description);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddPrinter(Printer printer)
        {
            int id = -1;
            var query = @"INSERT INTO printerInfo (PrinterInfoName,PrinterInfoIP, PrinterInfoDescription, place_idPlace, printermodel_idPrinterModel) 
                          VALUES (@PrinterInfoName,@PrinterInfoIP, @PrinterInfoDescription, @place_idPlace, @printermodel_idPrinterModel);";            

            var transcation = _sqlConnection.BeginTransaction();

            try
            {                
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PrinterInfoName", printer.Name);
                    cmd.Parameters.AddWithValue("@PrinterInfoIP", printer.Ip.ToString());
                    cmd.Parameters.AddWithValue("@PrinterInfoDescription", printer.Description);
                    cmd.Parameters.AddWithValue("@place_idPlace", printer.Place.Id);
                    cmd.Parameters.AddWithValue("@printermodel_idPrinterModel", printer.Model.Id);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }
            
                transcation.Commit();

                printer.Id = id;
                int idStatus = AddPrinterStatus(new PrinterStatus { Printer = printer });
                DataChanged?.Invoke(this, new DataChangedEventArgs(printer, DataChangedEventArgs.ChangeType.Add));
                return id;
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int AddPrinterModel(PrinterModel printerModel)
        {
            int id = -1;
            var query = @"INSERT INTO printermodel (PrinterModelName, PrinterModelMultiColor) VALUES (@PrinterModelName, @PrinterModelMultiColor);";
            var junctionToner = @"INSERT INTO printertoner (tonermodel_idTonerModel, printermodel_idPrinterModel) VALUES (@tonerId, @printerModelId);";
            var junctionDrum = @"INSERT INTO printerdrum (drummodel_idDrumModel, printermodel_idPrinterModel) VALUES (@drumId, @printerModelId);";
            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PrinterModelName", printerModel.Name);
                    cmd.Parameters.AddWithValue("@PrinterModelMultiColor", printerModel.IsColorSupported);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

                foreach(var toner in printerModel.Toners)
                {
                    using (var cmd = new MySqlCommand(junctionToner, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@tonerId", toner.Id);
                        cmd.Parameters.AddWithValue("@printerModelId", id);
                        cmd.ExecuteNonQuery();
                    }
                }

                foreach (var drum in printerModel.Drums)
                {
                    using (var cmd = new MySqlCommand(junctionDrum, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@drumId", drum.Id);
                        cmd.Parameters.AddWithValue("@printerModelId", id);
                        cmd.ExecuteNonQuery();
                    }
                }
            
                transcation.Commit();
                return id;
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int AddSupplier(Supplier supplier)
        {
            int id = -1;
            var query = @"INSERT INTO supplier (supplierName, supplierPhone, supplierEmail, supplierDescription) 
                          VALUES (@supplierName, @supplierPhone, @supplierEmail, @supplierDescription);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@supplierName", supplier.Name);
                    cmd.Parameters.AddWithValue("@supplierPhone", supplier.Phone);
                    cmd.Parameters.AddWithValue("@supplierEmail", supplier.Email);
                    cmd.Parameters.AddWithValue("@supplierDescription", supplier.Description);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddToner(Toner toner)
        {
            int id = -1;
            var query = @"INSERT INTO toner(TonerSerialNumber, TonerPrice, tonermodel_idTonerModel, document_idDocument) 
                          VALUES (@TonerSerialNumber, @TonerPrice, @tonermodel_idTonerModel, @document_idDocument);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@TonerSerialNumber", toner.SerialNumber);
                    cmd.Parameters.AddWithValue("@TonerPrice", toner.Price);
                    cmd.Parameters.AddWithValue("@tonermodel_idTonerModel", toner.Model.Id);
                    cmd.Parameters.AddWithValue("@document_idDocument", toner.Invoice.Id);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int AddTonerModel(TonerModel tonerModel)
        {
            int id = -1;
            var query = @"INSERT INTO tonermodel (TonerModelName, TonerModelColor, TonerModelThreshold) VALUES (@TonerModelName, @TonerModelColor, @TonerModelThreshold);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@TonerModelName", tonerModel.Name);
                    cmd.Parameters.AddWithValue("@TonerModelColor", Enum.GetName(typeof(TonerModel.Color), tonerModel.ColorType));
                    cmd.Parameters.AddWithValue("@TonerModelThreshold", tonerModel.Threshold);

                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }

            
                transcation.Commit();
                return id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public void Dispose()
        {
            _sqlConnection.Close();
            _sqlConnection.Dispose();
        }

        public int EditDepartment(Department department)
        {            
            var query = @"UPDATE department
                          SET DepartmentName=@DepartmentName, personinfo_idPersonInfo=@personinfo_idPersonInfo
                          WHERE idDepartment=@idDepartment;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DepartmentName", department.Name);
                    cmd.Parameters.AddWithValue("@personinfo_idPersonInfo", department.Supervisor.Id);
                    cmd.Parameters.AddWithValue("@idDepartment", department.Id);

                    cmd.ExecuteNonQuery();
                }

            
                transcation.Commit();
                return department.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditDrum(Drum drum)
        {            
            var query = @"UPDATE drum
                          SET DrumSerialNumber=@DrumSerialNumber, DrumPrice=@DrumPrice, document_idDocument=@document_idDocument, drummodel_idDrumModel=@drummodel_idDrumModel
                          WHERE idDrum=@idDrum;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DrumSerialNumber", drum.SerialNumber);
                    cmd.Parameters.AddWithValue("@DrumPrice", drum.Price);
                    cmd.Parameters.AddWithValue("@document_idDocument", drum.Invoice.Id);
                    cmd.Parameters.AddWithValue("@drummodel_idDrumModel", drum.Model.Id);                
                    cmd.Parameters.AddWithValue("@idDrum", drum.Id);

                    cmd.ExecuteNonQuery();
                }

            
                transcation.Commit();
                return drum.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditDrumModel(DrumModel drumModel)
        {           
            var query = @"UPDATE drummodel
                          SET DrumModelName=@DrumModelName, DrumModelThreshold=@DrumModelThreshold
                          WHERE idDrumModel=@idDrumModel;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DrumModelName", drumModel.Name);
                    cmd.Parameters.AddWithValue("@DrumModelThreshold", drumModel.Threshold);
                    cmd.Parameters.AddWithValue("@idDrumModel", drumModel.Id);

                    cmd.ExecuteNonQuery();
                }

            
                transcation.Commit();
                return drumModel.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditInvoice(Invoice invoice)
        {           
            var query = @"UPDATE document
                          SET DocumentNumber=@DocumentNumber, DocumentPrice=@DocumentPrice, DocumentDate=@DocumentDate, supplier_idsupplier=@supplier_idsupplier
                          WHERE idDocument=@idDocument;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@DocumentNumber", invoice.Number);
                    cmd.Parameters.AddWithValue("@DocumentPrice", invoice.Price);
                    cmd.Parameters.AddWithValue("@DocumentDate", invoice.Date);
                    cmd.Parameters.AddWithValue("@supplier_idsupplier", invoice.Supplier.Id);
                    cmd.Parameters.AddWithValue("@idDocument", invoice.Id);

                    cmd.ExecuteNonQuery();
                }

            
                transcation.Commit();
                return invoice.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditOrder(Order order)
        {            
            var query = @"UPDATE orderinfo
                          SET OrderInfoDate=@OrderInfoDate, personinfo_idPersonInfo=@personinfo_idPersonInfo, department_idDepartment=@department_idDepartment
                          WHERE idOrderInfo=@idOrderInfo;";

            var resetTonerQuery = @"UPDATE toner SET orderinfo_idOrderInfo=null WHERE orderinfo_idOrderInfo=@idOrderInfo;";
            var resetDrumQuery = @"UPDATE drum SET orderinfo_idOrderInfo=null WHERE orderinfo_idOrderInfo=@idOrderInfo;";

            var updateTonerQuery = @"UPDATE toner SET orderinfo_idOrderInfo=@idOrderInfo WHERE idToner=@tonerId;";
            var updateDrumQuery = @"UPDATE drum SET orderinfo_idOrderInfo=@idOrderInfo WHERE idDrum=@drumId;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(resetDrumQuery, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idOrderInfo", order.Id);                   
                    cmd.ExecuteNonQuery();
                }
                using (var cmd = new MySqlCommand(resetTonerQuery, _sqlConnection))
                {
                    cmd.Transaction = transcation;                    
                    cmd.Parameters.AddWithValue("@idOrderInfo", order.Id);
                    cmd.ExecuteNonQuery();
                }

                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@OrderInfoDate", order.Date);
                    cmd.Parameters.AddWithValue("@personinfo_idPersonInfo", order.Client.Id);
                    cmd.Parameters.AddWithValue("@department_idDepartment", order.Department.Id);
                    cmd.Parameters.AddWithValue("@idOrderInfo", order.Id);
                    cmd.ExecuteNonQuery();
                }

                foreach(var toner in order.Toners)
                {
                    using (var cmd = new MySqlCommand(updateTonerQuery, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@idOrderInfo", order.Id);
                        cmd.Parameters.AddWithValue("@tonerId", toner.Id);
                        cmd.ExecuteNonQuery();
                    }
                }

                foreach (var drum in order.Drums)
                {
                    using (var cmd = new MySqlCommand(updateDrumQuery, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@idOrderInfo", order.Id);
                        cmd.Parameters.AddWithValue("@drumId", drum.Id);
                        cmd.ExecuteNonQuery();
                    }
                }


                transcation.Commit();
                return order.Id;
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int EditPerson(Person person)
        {          
            var query = @"UPDATE personinfo
                          SET PersonInfoName=@PersonInfoName, PersonInfoSurname=@PersonInfoSurname, department_idDepartment=@department_idDepartment
                          WHERE idPersonInfo=@idPersonInfo;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PersonInfoName", person.Name);
                    cmd.Parameters.AddWithValue("@PersonInfoSurname", person.Surname);
                    if (person.Department != null)
                        cmd.Parameters.AddWithValue("@department_idDepartment", person.Department.Id);
                    else
                        cmd.Parameters.AddWithValue("@department_idDepartment", DBNull.Value);
                    cmd.Parameters.AddWithValue("@idPersonInfo", person.Id);

                    cmd.ExecuteNonQuery();
                }

            
                transcation.Commit();
                return person.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditPlace(Place place)
        {           
            var query = @"UPDATE place
                          SET PlaceName=@PlaceName, PlaceFloor=@PlaceFloor, PlaceRoom=@PlaceRoom, PlaceDescription=@PlaceDescription
                          WHERE idPlace=@idPlace;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PlaceName", place.Name);
                    cmd.Parameters.AddWithValue("@PlaceFloor", place.Floor);
                    cmd.Parameters.AddWithValue("@PlaceRoom", place.Room);
                    cmd.Parameters.AddWithValue("@PlaceDescription", place.Description);
                    cmd.Parameters.AddWithValue("@idPlace", place.Id);

                    cmd.ExecuteNonQuery();
                }

            
                transcation.Commit();
                return place.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditPrinter(Printer printer)
        {            
            var query = @"UPDATE printerinfo
                          SET PrinterInfoName = @PrinterInfoName, PrinterInfoIP=@PrinterInfoIP, PrinterInfoDescription=@PrinterInfoDescription, place_idPlace=@place_idPlace, printermodel_idPrinterModel=@printermodel_idPrinterModel
                          WHERE idPrinterInfo=@idPrinterInfo;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PrinterInfoName", printer.Name);
                    cmd.Parameters.AddWithValue("@PrinterInfoIP", printer.Ip.ToString());
                    cmd.Parameters.AddWithValue("@PrinterInfoDescription", printer.Description);
                    cmd.Parameters.AddWithValue("@place_idPlace", printer.Place.Id);
                    cmd.Parameters.AddWithValue("@printermodel_idPrinterModel", printer.Model.Id);
                    cmd.Parameters.AddWithValue("@idPrinterInfo", printer.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                DataChanged?.Invoke(this, new DataChangedEventArgs(printer, DataChangedEventArgs.ChangeType.Edit));
                return printer.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditPrinterStatus(PrinterStatus printerStatus)
        {
            int id = -1;
            var query = @"UPDATE printerstatus
                        INNER JOIN printerinfo ON printerinfo_idPrinterInfo = idPrinterInfo
                        SET PrinterStatusTonerBlack=@PrinterStatusTonerBlack
                        WHERE PrinterInfoIP LIKE @printerIp;";

            var transcation = _sqlConnection.BeginTransaction();
            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;                    
                    cmd.Parameters.AddWithValue("@PrinterStatusTonerBlack", printerStatus.Black);
                    cmd.Parameters.AddWithValue("@printerIp", printerStatus.Printer.Ip.ToString());
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;
                }


                transcation.Commit();
                DataChanged?.Invoke(this, new DataChangedEventArgs(printerStatus, DataChangedEventArgs.ChangeType.Edit));
                return id;
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int EditPrinterModel(PrinterModel printerModel)
        {           
            var query = @"UPDATE printermodel
                          SET PrinterModelName=@PrinterModelName, PrinterModelMultiColor=@PrinterModelMultiColor
                          WHERE idPrinterModel=@idPrinterModel;";
            var junctionDrumClearQuery = @"DELETE FROM printerdrum WHERE printermodel_idPrinterModel=@printerModelId;";
            var junctionTonerClearQuery = @"DELETE FROM printertoner WHERE printermodel_idPrinterModel=@printerModelId;";
            var junctionTonerQuery = @"INSERT INTO printertoner (tonermodel_idTonerModel, printermodel_idPrinterModel) VALUES (@tonerId, @printerModelId);";
            var junctionDrumQuery = @"INSERT INTO printerdrum (drummodel_idDrumModel, printermodel_idPrinterModel) VALUES (@drumId, @printerModelId);";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                //Clear junction tables by ID
                using (var cmd = new MySqlCommand(junctionTonerClearQuery, _sqlConnection))
                {
                    cmd.Transaction = transcation;                    
                    cmd.Parameters.AddWithValue("@printerModelId", printerModel.Id);

                    cmd.ExecuteNonQuery();
                }
                using (var cmd = new MySqlCommand(junctionDrumClearQuery, _sqlConnection))
                {
                    cmd.Transaction = transcation;                    
                    cmd.Parameters.AddWithValue("@printerModelId", printerModel.Id);

                    cmd.ExecuteNonQuery();
                }

                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@PrinterModelName", printerModel.Name);
                    cmd.Parameters.AddWithValue("@PrinterModelMultiColor", printerModel.IsColorSupported);
                    cmd.Parameters.AddWithValue("@idPrinterModel", printerModel.Id);
                    cmd.ExecuteNonQuery();
                }                

                //Insert new values
                foreach (var toner in printerModel.Toners)
                {
                    using (var cmd = new MySqlCommand(junctionTonerQuery, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@tonerId", toner.Id);
                        cmd.Parameters.AddWithValue("@printerModelId", printerModel.Id);
                        cmd.ExecuteNonQuery();
                    }
                }
                foreach (var drum in printerModel.Drums)
                {
                    using (var cmd = new MySqlCommand(junctionDrumQuery, _sqlConnection))
                    {
                        cmd.Transaction = transcation;
                        cmd.Parameters.AddWithValue("@drumId", drum.Id);
                        cmd.Parameters.AddWithValue("@printerModelId", printerModel.Id);
                        cmd.ExecuteNonQuery();
                    }
                }
            
            
                transcation.Commit();
                return printerModel.Id;
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int EditSupplier(Supplier supplier)
        {         
            var query = @"UPDATE supplier
                          SET supplierName=@supplierName, supplierPhone=@supplierPhone, supplierEmail=@supplierEmail, supplierDescription=@supplierDescription
                          WHERE idsupplier=@idsupplier;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@supplierName", supplier.Name);
                    cmd.Parameters.AddWithValue("@supplierPhone", supplier.Phone);
                    cmd.Parameters.AddWithValue("@supplierEmail", supplier.Email);
                    cmd.Parameters.AddWithValue("@supplierDescription", supplier.Description);
                    cmd.Parameters.AddWithValue("@idsupplier", supplier.Id);
                    cmd.ExecuteNonQuery();                
                }
            
                transcation.Commit();
                return supplier.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditToner(Toner toner)
        {           
            var query = @"UPDATE toner
                          SET tonermodel_idTonerModel=@tonermodel_idTonerModel, document_idDocument=@document_idDocument, TonerPrice=@TonerPrice, TonerSerialNumber=@TonerSerialNumber
                          WHERE idToner=@idToner;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@tonermodel_idTonerModel", toner.Model.Id);
                    cmd.Parameters.AddWithValue("@document_idDocument", toner.Invoice.Id);
                    cmd.Parameters.AddWithValue("@TonerPrice", toner.Price);
                    cmd.Parameters.AddWithValue("@TonerSerialNumber", toner.SerialNumber);
                    cmd.Parameters.AddWithValue("@idToner", toner.Id);
                    cmd.ExecuteNonQuery();               
                }

            
                transcation.Commit();
                return toner.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int EditTonerModel(TonerModel tonerModel)
        {            
            var query = @"UPDATE tonermodel
                          SET TonerModelName=@TonerModelName, TonerModelColor=@TonerModelColor, TonerModelThreshold=@TonerModelThreshold
                          WHERE idTonerModel=@idTonerModel;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@TonerModelName", tonerModel.Name);
                    cmd.Parameters.AddWithValue("@TonerModelColor", Enum.GetName(typeof(TonerModel.Color), tonerModel.ColorType));
                    cmd.Parameters.AddWithValue("@idTonerModel", tonerModel.Id);
                    cmd.Parameters.AddWithValue("@TonerModelThreshold", tonerModel.Threshold);
                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return tonerModel.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public List<Department> GetDepartments()
        {
            var query = @"SELECT department.* , personinfo.PersonInfoName, personinfo.PersonInfoSurname
                            FROM department
                            LEFT JOIN personinfo
                            ON department.personinfo_idPersonInfo=personinfo.idPersonInfo;";
            var result = new List<Department>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Department();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        if (!reader.IsDBNull(2))
                        {
                            item.Supervisor = new Person
                            {
                                Id = reader.GetInt32(2),
                                Name = reader.GetString(3),
                                Surname = reader.GetString(4)
                            };
                        }
                        
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<DrumModel> GetDrumModels()
        {
            var query = @"SELECT * FROM drummodel;";
            var result = new List<DrumModel>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new DrumModel();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.Threshold = reader.GetInt32(2);
                        result.Add(item);
                    }
                }                                    
            }
            return result;
        }

        public List<Drum> GetDrums()
        {
            var query = @"SELECT * FROM drum;";
            var result = new List<Drum>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Drum();
                        item.Id = reader.GetInt32(0);
                        item.SerialNumber = reader.GetString(1);
                        item.Price = reader.GetDouble(2); 
                        if (!reader.IsDBNull(3))
                        {
                            item.Invoice = new Invoice
                            {
                                Id = reader.GetInt32(3)
                            };
                        }
                        if (!reader.IsDBNull(4))
                        {
                            item.Model = new DrumModel
                            {
                                Id = reader.GetInt32(4)
                            };
                        }
                        if (!reader.IsDBNull(5))
                        {
                            item.Order = new Order
                            {
                                Id = reader.GetInt32(5)
                            };
                        }
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Invoice> GetInvoices()
        {
            var query = @"SELECT document.* , supplier.supplierName
                        FROM document
                        INNER JOIN supplier
                        ON document.supplier_idsupplier=supplier.idsupplier;";
            var result = new List<Invoice>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Invoice();
                        item.Id = reader.GetInt32(0);
                        item.Number = reader.GetString(1);
                        item.Price = reader.GetDouble(2);
                        item.Date = reader.GetDateTime(3);
                        if (!reader.IsDBNull(4))
                        {
                            item.Supplier = new Supplier
                            {
                                Id = reader.GetInt32(4),
                                Name = reader.GetString(5)
                            };
                        }
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Order> GetOrders()
        {
            var query = @"SELECT orderinfo.*, personinfo.PersonInfoName, personinfo.PersonInfoSurname, department.DepartmentName
                        FROM (( orderinfo
                        LEFT JOIN personinfo ON orderinfo.personinfo_idPersonInfo = personinfo.idPersonInfo)
                        LEFT JOIN department ON orderinfo.department_idDepartment = department.idDepartment);";
            var result = new List<Order>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Order();
                        item.Id = reader.GetInt32(0);                        
                        item.Date = reader.GetDateTime(1);
                        
                        if (!reader.IsDBNull(2))
                        {
                            item.Client = new Person
                            {
                                Id = reader.GetInt32(2),
                                Name = reader.GetString(4),
                                Surname = reader.GetString(5)
                            };
                        }

                        if (!reader.IsDBNull(3))
                        {
                            item.Department = new Department
                            {
                                Id = reader.GetInt32(3)
                            };
                        }
                        
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Person> GetPeople()
        {
            var query = @"SELECT personinfo.*, department.DepartmentName
                        FROM personinfo
                        LEFT JOIN department ON personinfo.department_idDepartment = department.idDepartment;";
            var result = new List<Person>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Person();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.Surname = reader.GetString(2);
                        
                        if (!reader.IsDBNull(3))
                        {
                            item.Department = new Department
                            {
                                Id = reader.IsDBNull(3) ? -1 : reader.GetInt32(3),
                                Name = reader.IsDBNull(4) ? String.Empty : reader.GetString(4)
                            };
                        }
                        
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Place> GetPlaces()
        {
            var query = @"SELECT * FROM place;";
            var result = new List<Place>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Place();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.Floor = reader.GetInt32(2);
                        item.Room = reader.GetString(3);
                        item.Description = reader.IsDBNull(4) ? String.Empty : reader.GetString(4);
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<PrinterModel> GetPrinterModels()
        {
            var query = @"SELECT * FROM printermodel;";
            var result = new List<PrinterModel>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new PrinterModel();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.IsColorSupported = reader.GetBoolean(2);                        
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Printer> GetPrinters()
        {
            var query = @"SELECT printerinfo.*, place.PlaceName, printermodel.PrinterModelName
                        FROM printerinfo
                        INNER JOIN place ON printerinfo.place_idPlace = place.idPlace
                        INNER JOIN printermodel ON printerinfo.printermodel_idPrinterModel = printermodel.idPrinterModel;";
            var result = new List<Printer>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Printer();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.Ip = IPAddress.Parse(reader.GetString(2));
                        item.Description = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);

                        if (!reader.IsDBNull(4))                        
                            item.Place = new Place { Id = reader.GetInt32(4), Name = reader.GetString(6) };
                        

                        if (!reader.IsDBNull(5))                        
                            item.Model = new PrinterModel { Id = reader.GetInt32(5), Name = reader.GetString(7) };

                        result.Add(item);
                    }
                }                    
            }
            return result;
        }

        public List<Supplier> GetSuppliers()
        {
            var query = @"SELECT * FROM supplier;";
            var result = new List<Supplier>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Supplier();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.Phone = reader.GetString(2);
                        item.Email = reader.GetString(3);
                        item.Description = reader.IsDBNull(4) ? String.Empty : reader.GetString(4);
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<TonerModel> GetTonerModels()
        {
            var query = @"SELECT * FROM tonermodel;";
            var result = new List<TonerModel>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new TonerModel();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.ColorType = (TonerModel.Color)Enum.Parse(typeof(TonerModel.Color), reader.GetString(2));
                        item.Threshold = reader.GetInt32(3);
                        result.Add(item);
                    }
                }                                    
            }
            return result;
        }

        public List<Toner> GetToners()
        {
            var query = @"SELECT * FROM toner;";
            var result = new List<Toner>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new Toner();
                        item.Id = reader.GetInt32(0);
                        item.SerialNumber = reader.GetString(1);
                        item.Price = reader.GetDouble(2);
                        if (!reader.IsDBNull(3))
                            item.Model = new TonerModel { Id = reader.GetInt32(3) };
                        if (!reader.IsDBNull(4))
                            item.Invoice = new Invoice { Id = reader.GetInt32(4) };
                        if (!reader.IsDBNull(5))
                            item.Order = new Order { Id = reader.GetInt32(5) };
                        
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public void RaiseDataChanged(object sender, DataChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public int RemoveDepartment(Department department)
        {            
            var query = @"DELETE FROM department
                          WHERE idDepartment=@idDepartment;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idDepartment", department.Id);                

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return department.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemoveDrum(Drum drum)
        {
            var query = @"DELETE FROM drum
                          WHERE idDrum=@idDrum;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idDrum", drum.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return drum.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemoveDrumModel(DrumModel drumModel)
        {
            var query = @"DELETE FROM drummodel
                          WHERE idDrumModel=@idDrumModel;";

            var junctionClear = "DELETE FROM printerdrum WHERE drummodel_idDrumModel = @drumModelId;";
            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(junctionClear, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@drumModelId", drumModel.Id);

                    cmd.ExecuteNonQuery();
                }

                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idDrumModel", drumModel.Id);                

                    cmd.ExecuteNonQuery();
                }
                        
                transcation.Commit();
                return drumModel.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }
        
        public int RemoveInvoice(Invoice invoice)
        {
            var query = @"DELETE FROM document
                          WHERE idDocument=@idDocument;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idDocument", invoice.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return invoice.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemoveOrder(Order order)
        {
            var query = @"DELETE FROM orderinfo
                          WHERE idOrderInfo=@idOrderInfo;";

            var clearDrum = "UPDATE drum SET orderinfo_idOrderInfo=null WHERE orderinfo_idOrderInfo=@orderId;";
            var clearToner = "UPDATE toner SET orderinfo_idOrderInfo=null WHERE orderinfo_idOrderInfo=@orderId;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(clearDrum, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@orderId", order.Id);

                    cmd.ExecuteNonQuery();
                }
                using (var cmd = new MySqlCommand(clearToner, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@orderId", order.Id);

                    cmd.ExecuteNonQuery();
                }
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idOrderInfo", order.Id);

                    cmd.ExecuteNonQuery();
                }

                
            
                transcation.Commit();
                return order.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemovePerson(Person person)
        {
            var query = @"DELETE FROM personinfo
                          WHERE idPersonInfo=@idPersonInfo;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idPersonInfo", person.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return person.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemovePlace(Place place)
        {
            var query = @"DELETE FROM place
                          WHERE idPlace=@idPlace;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idPlace", place.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return place.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemovePrinter(Printer printer)
        {
            var query = @"DELETE FROM printerinfo 
                          WHERE idPrinterInfo=@idPrinterInfo;";

            RemovePrinterStatus(printer);

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idPrinterInfo", printer.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return printer.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemovePrinterModel(PrinterModel printerModel)
        {
            var query = @"DELETE FROM printermodel
                          WHERE idPrinterModel=@idPrinterModel;";
            var junctionDrumClear = "DELETE FROM printerdrum WHERE printermodel_idPrinterModel=@printerModelId;";
            var junctionTonerClear = "DELETE FROM printertoner WHERE printermodel_idPrinterModel=@printerModelId;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(junctionDrumClear, _sqlConnection))
                {
                    cmd.Transaction = transcation;                    
                    cmd.Parameters.AddWithValue("@printerModelId", printerModel.Id);

                    cmd.ExecuteNonQuery();
                }

                using (var cmd = new MySqlCommand(junctionTonerClear, _sqlConnection))
                {
                    cmd.Transaction = transcation;                    
                    cmd.Parameters.AddWithValue("@printerModelId", printerModel.Id);

                    cmd.ExecuteNonQuery();
                }

                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idPrinterModel", printerModel.Id);

                    cmd.ExecuteNonQuery();
                }            
            
                transcation.Commit();
                return printerModel.Id;
            }
            catch(Exception ex)
            {
                Debug.Write(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }

        public int RemoveSupplier(Supplier supplier)
        {
            var query = @"DELETE FROM supplier
                          WHERE idsupplier=@idsupplier;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idsupplier", supplier.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return supplier.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemoveToner(Toner toner)
        {
            var query = @"DELETE FROM toner
                          WHERE idToner=@idToner;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idToner", toner.Id);

                    cmd.ExecuteNonQuery();
                }
            
                transcation.Commit();
                return toner.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public int RemoveTonerModel(TonerModel tonerModel)
        {
            var query = @"DELETE FROM tonermodel 
                          WHERE idTonerModel=@idTonerModel;";

            var junctionClear = @"DELETE FROM printertoner WHERE tonermodel_idTonerModel=@tonerModelId;";

            var transcation = _sqlConnection.BeginTransaction();

            try
            {
                using (var cmd = new MySqlCommand(junctionClear, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@tonerModelId", tonerModel.Id);

                    cmd.ExecuteNonQuery();
                }

                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idTonerModel", tonerModel.Id);

                    cmd.ExecuteNonQuery();
                }            
            
                transcation.Commit();
                return tonerModel.Id;
            }
            catch
            {
                transcation.Rollback();
                return -1;
            }
        }

        public List<DrumModel> GetCompatibleDrums(int printerId)
        {
            var query = @"SELECT drummodel.* 
                        FROM ((printerdrum
                        INNER JOIN printermodel ON printerdrum.printermodel_idPrinterModel = printermodel.idPrinterModel)
                        INNER JOIN drummodel ON printerdrum.drummodel_idDrumModel = drummodel.idDrumModel)
                        WHERE printermodel.idPrinterModel = @idPrinterModel;";
            var result = new List<DrumModel>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                cmd.Parameters.AddWithValue("@idPrinterModel", printerId);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new DrumModel();
                        item.Id = reader.GetInt32(0);                        
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<TonerModel> GetCompatibleToners(int printerId)
        {
            var query = @"SELECT tonermodel.* 
                            FROM ((printertoner
                            INNER JOIN printermodel ON printertoner.printermodel_idPrinterModel = printermodel.idPrinterModel)
                            INNER JOIN tonermodel ON printertoner.tonermodel_idTonerModel = tonermodel.idTonerModel)
                            WHERE printermodel.idPrinterModel = @idPrinterModel";
            var result = new List<TonerModel>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {
                cmd.Parameters.AddWithValue("@idPrinterModel", printerId);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new TonerModel();
                        item.Id = reader.GetInt32(0);
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<PrinterStatus> GetPrinterStatus()
        {
            var query = @"SELECT printerstatus.*, printerinfo.PrinterInfoIP FROM printerstatus
                          INNER JOIN printerinfo ON printerinfo_idPrinterInfo = idPrinterInfo;";

            var result = new List<PrinterStatus>();
            using (var cmd = new MySqlCommand(query, _sqlConnection))
            {                
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = new PrinterStatus();
                        item.Id = reader.GetInt32(0);
                        if (!reader.IsDBNull(3))
                            item.Black = reader.GetInt32(3);
                        item.Printer = new Printer { Id = reader.GetInt32(7), Ip = IPAddress.Parse(reader.GetString(8)) };
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public int RemovePrinterStatus(Printer printer)
        {
            int id = -1;
            var query = @"DELETE FROM printerstatus WHERE printerinfo_idPrinterInfo=@idPrinterInfo;";

            var transcation = _sqlConnection.BeginTransaction();
            try
            {
                using (var cmd = new MySqlCommand(query, _sqlConnection))
                {
                    cmd.Transaction = transcation;
                    cmd.Parameters.AddWithValue("@idPrinterInfo", printer.Id);                    
                    cmd.ExecuteNonQuery();
                    id = (int)cmd.LastInsertedId;                    
                }


                transcation.Commit();
                DataChanged?.Invoke(this, new DataChangedEventArgs(printer, DataChangedEventArgs.ChangeType.Delete));
                return id;
            }
            catch(Exception ex)
            {
                Debug.Write(ex.Message);
                transcation.Rollback();
                return -1;
            }
        }
    }
}
